# Discord
https://discord.gg/AYf7nWF

# Preview
https://streamable.com/c3f0sy


# Description
- Includes 3 different shops with multiple locations (Benny's Motorworks, Beeker's Garage and LS Customs).
- Allows you to customize the vehicles appearance.
- Allows you to purchase vehicle upgrades for your vehicles.
- Repair function for repairing your vehicle.
- Repair price dependent on the damage the vehicle has taken.
- Sound effects when repairing/purchasing parts.
- Clean UI with a different title for each version of the shops.
- Multiple players can enter at the same time.



# Installation
Add to folder '[esx]'
Write 'start Reload-Bennys' in your server.cfg


#LICENSE
This mod was made by Reload with the intention to distribute.
He and he alone reserves the right to sell this script. Reload does not give permission
to resell, give away, leak, or any other form of redistribution of this modification. Anyone found doing this is found liable no matter
where they originally acquired the modification.
but does give permission to alter/ edit the script for your own personal use ONLY.
