INSERT INTO `jobs` (name, label) VALUES
	('garbage', 'Garbage Job')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	('garbage',0,'employee','Employee',20,'{}','{}')
;


INSERT INTO items (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES ('metalscrap', 'Scrap Metal', 10, 0, 1);
INSERT INTO items (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES ('plastic', 'Plastic', 15, 0, 1);
INSERT INTO items (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES ('aluminum', 'Aluminum', 10, 0, 1);
INSERT INTO items (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES ('steel', 'Steel', 10, 0, 1);
INSERT INTO items (`name`, `label`, `limit`, `rare`, `can_remove`) VALUES ('glass', 'Glass', 15, 0, 1);