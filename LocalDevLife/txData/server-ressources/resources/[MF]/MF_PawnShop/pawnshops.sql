CREATE TABLE IF NOT EXISTS `pawnshops` (
  `shopdata` longtext,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `items` (`name`, `label`, `limit`) VALUES
  ('weed','Weed',-1),
  ('meth','Meth',-1),
  ('rolex','Rolex',-1),
  ('lockpick','Lockpick',-1);
