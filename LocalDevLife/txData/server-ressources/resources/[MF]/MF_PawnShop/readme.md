-- ModFreakz
-- For support, previews and showcases, head to https://discord.gg/ukgQa5K

Requirements
- ESX

Installation
- Extract to resources folder
- Start in server.cfg
- If sql file provided, import it.
- Make sure you read the config for things you might need to change.
- Make sure requirements are installed and started in server.cfg (if not provided, please ask via discord).
- Make sure you're added to webhook. If you have no idea what that is, head to the link above and create a ticket.

Notes
- To add items/remove items from the shop, just edit the config table.
- Use existing items as an example.