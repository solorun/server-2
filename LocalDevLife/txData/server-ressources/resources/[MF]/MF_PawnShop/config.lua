-- ModFreakz
-- For support, previews and showcases, head to https://discord.gg/ukgQa5K

MF_PawnShop = {}
local MFP = MF_PawnShop

MFP.Version = '1.0.10'

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj; end)
Citizen.CreateThread(function(...)
  while not ESX do 
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj; end) 
    Citizen.Wait(0)
  end
end)

function MFP:GetShops()
  return {
    [1] = {
      title = 'Drug Shop',
      text  = 'Press [~r~E~s~] to interact with the Drug Shop.',
      loc   = {x=-1172.01,y=-1571.95,z=4.66},
      blip  = false,

      buy = {
        [1] = {
          label       = 'Weed',
          item        = 'weed',
          price       = 200,
          max         = 50,
          startcount  = 0,
        },

        [2] = {
          label       = 'Meth',
          item        = 'meth',
          price       = 100,
          max         = 10,
          startcount  = 0,
        },
      },

      sell = {
        [1] = {
          label   = 'Weed',
          item    = 'weed',
          price   = 100,
          max     = 50,
        },

        [2] = {
          label   = 'Meth',
          item    = 'meth',
          price   = 50,
          max     = 10,
        },
      },
    },
    [2] = {
      title = 'Pawn Shop',
      text  = 'Press [~r~E~s~] to interact with the Pawn Shop.',
      loc   = {x=412.23,y=315.11,z=103.13},
      blip  = true,

      buy = {
        [1] = {
          label       = 'Lockpick',
          item        = 'lockpick',
          price       = 100,
          max         = 50,
          startcount  = 5,
        },

        [2] = {
          label       = 'Rolex',
          item        = 'rolex',
          price       = 100,
          max         = 10,
          startcount  = 0,
        },
		
		[3] = {
          label       = 'Oxycutter',
          item        = 'oxycutter',
          price       = 100,
          max         = 10,
          startcount  = 0,
        },
		
      },

      sell = {
        [1] = {
          label   = 'Lockpick',
          item    = 'lockpick',
          price   = 500,
          max     = 50,
        },

        [2] = {
          label   = 'Rolex',
          item    = 'rolex',
          price   = 200,
          max     = 10,
        },
		
		[3] = {
          label       = 'Oxycutter',
          item        = 'oxycutter',
          price       = 100,
          max         = 10,
        },
		
      },
    },
    [3] = {
      title   = 'Black Market',
      text    = 'Press [~r~E~s~] to interact with the Black Market.',
      loc     = {x=830.43,y=-2171.67,z=30.27},
      blip    = false,

      buy = {
        [1] = {
          label       = 'Minigun',
          item        = 'weapon_minigun',
          price       = 50000,
          max         = 1,
          startcount  = 0,
          weapon      = true,
        },

        [2] = {
          label       = 'Assault Shotgun',
          item        = 'weapon_assaultshotgun',
          price       = 15000,
          max         = 5,
          startcount  = 0,
          weapon      = true,
        },

        [3] = {
          label       = 'Pump Shotgun',
          item        = 'weapon_pumpshotgun',
          price       = 15000,
          max         = 5,
          startcount  = 0,
          weapon      = true,
        },
      },

      sell = {
        [1] = {
          label   = 'Minigun',
          item    = 'weapon_minigun',
          price   = 1000,
          max     = 1,
          weapon  = true,
        },

        [2] = {
          label   = 'Assault Shotgun',
          item    = 'weapon_assaultshotgun',
          price   = 5000,
          max     = 5,
          weapon  = true,
        },

        [3] = {
          label   = 'Pump Shotgun',
          item    = 'weapon_pumpshotgun',
          price   = 5000,
          max     = 5,
          weapon  = true,
        },
      },
    },
  }
end