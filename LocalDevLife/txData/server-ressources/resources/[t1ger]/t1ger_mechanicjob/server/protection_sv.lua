local authoriseID = 'Love U Tiger | ilAn#0393'

        -- Get Owned Companies:
        ESX.RegisterServerCallback('t1ger_mechanicjob:getTakenShops',function(source, cb)
            local ownedShop = {}
            local dataFetched = false
            MySQL.Async.fetchAll("SELECT * FROM t1ger_mechanic",{}, function(data)
                if #data > 0 then 
                    for k,v in pairs(data) do
                        table.insert(ownedShop,{id = v.shopID, name = v.name})
                        if k == #data then dataFetched = true end
                    end
                else
                    dataFetched = true
                end
                while not dataFetched do 
                    Wait(5)
                end
                if dataFetched then 
                    cb(ownedShop)
                end
            end)
        end)
        
        -- Callback to Get accounts:
        ESX.RegisterServerCallback('t1ger_mechanicjob:getShopAccounts', function(source, cb, id)
            MySQL.Async.fetchAll("SELECT money FROM t1ger_mechanic WHERE shopID = @shopID", {['@shopID'] = id}, function(data)
                if data[1].money ~= nil then 
                    cb(data[1].money)
                end
            end)
        end)

        -- Sync Vehicle Body:
        RegisterServerEvent('t1ger_mechanicjob:syncVehicleBodySV')
        AddEventHandler('t1ger_mechanicjob:syncVehicleBodySV', function(plate)
            TriggerClientEvent('t1ger_mechanicjob:syncVehicleBodyCL', -1, plate)
        end)

        -- Lift Features:
        RegisterServerEvent('t1ger_mechanicjob:liftStateSV') 
        AddEventHandler('t1ger_mechanicjob:liftStateSV', function(k, id, val, vehicle, state, player) 
            local xPlayer = ESX.GetPlayerFromId(source)
            local identifier = nil
            if player ~= nil then 
                identifier = xPlayer.getIdentifier()
            end
            Config.MechanicShops[k].lifts[id] = val
            Config.MechanicShops[k].lifts[id].currentVeh = vehicle
            Config.MechanicShops[k].lifts[id].inUse = state
            Config.MechanicShops[k].lifts[id].player = identifier
            Citizen.Wait(100)
            TriggerClientEvent('t1ger_mechanicjob:liftStateCL', -1, k, id, val, vehicle, state, identifier)
        end)

        RegisterServerEvent('t1ger_mechanicjob:JobDataSV')
        AddEventHandler('t1ger_mechanicjob:JobDataSV',function(data)
            TriggerClientEvent('t1ger_mechanicjob:JobDataCL', -1, data)
        end)

        function round(num, numDecimalPlaces)
            local mult = 10^(numDecimalPlaces or 0)
            return math.floor(num * mult + 0.5) / mult
        end

RegisterServerEvent('loaf_test:getAccess')
AddEventHandler('loaf_test:getAccess', function()
    local src = source
    TriggerClientEvent('loaf_test:setAccess', src, true)
end)