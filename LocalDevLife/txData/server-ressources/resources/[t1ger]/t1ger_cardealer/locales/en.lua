-------------------------------------
------- Created by T1GER#9080 -------
------------------------------------- 

Locales['en'] = {
  -- BOSS MENU:
  ['boss_menu']             	= '~r~[E]~s~ FOR AT AaBNE ~y~BOSS MENUEN~s~',
  ['employee_menu']            	= 'Ansatte',
  ['accounts_menu']           	= 'Konto',
  ['boss_menu_title']           = 'Chef ting',
  
  -- EMPLOYEES:
  ['btn_employee_list']         = 'Ansatte',
  ['btn_employee_reqruit']      = 'Rekruter ansatte',
  ['employee_menu_title']       = 'Styr ansatte',
  ['employee']               	= 'Ansatte',
  ['grade']                  	= 'Job grad',
  ['actions']                	= 'Handlinger',
  ['fire']                   	= 'Fyr',
  ['promote']                	= 'Opranger',
  ['promote_employee']       	= 'Opranger: %s',
  ['you_have_promoted']      	= 'Du opranger: %s to %s',
  ['you_have_fired']         	= 'Du fyrede: %s',
  ['you_have_hired']         	= 'Du ansatte: %s',
  ['you_have_been_hired']    	= 'Du blev ansat som %s',
  ['you_have_been_fired']    	= 'Du blev fyret fra dit job',
  ['you_have_been_promoted'] 	= 'Du er blevet oprangeret!',
  ['recruit']                	= 'Rekrutter',
  ['recruiting']             	= 'Rekruttering',
  ['recruit_player'] 			= 'Rekruttere: %s?',
  ['yes']                    	= 'Ja',
  ['no']                    	= 'Nej',
  
  -- ACCOUNTS:
  ['invalid_amount']            = 'Ugyldigt beloeb',
  ['btn_withdraw']            	= 'Traek penge ud',
  ['btn_deposit']            	= 'Indsaet penge',
  ['accounts_menu_title']       = 'PDM konto: $%s',
  ['amount_withdraw']           = 'Maengde der skal traekkes ud?',
  ['amount_deposit']           	= 'Maengde der skal saettes ind?',
  ['withdrew_amount']           = 'Du ~r~haevede~s~ ~y~$%s~s~ fra ~b~kontoen~s~',
  ['deposited_amount']          = 'du ~g~indsatte~s~ ~y~$%s~s~ i ~b~kontoen~s~',
  ['progbar_withdrawing']       = 'Traek penge',
  ['progbar_depositing']        = 'INDSaeTTER PENGE',
  ['not_enough_money']        	= 'Ikke nok penge',
  
  -- SHOP MENU:
  ['open_shop_menu']            = '~r~[E]~s~ For at Aabne ~y~MENUEN~s~',
  ['veh_catalog_btn']           = 'Se katalog',
  ['billing_btn']            	= 'Finansieringsregninger',
  ['sell_veh_btn']            	= 'Saelg koeretoej',
  ['shop_menu_title']           = 'Shop hovedmenu',
  ['confirm_veh_buy']           = 'Bekraeft koeb: %s for: $%s',
  ['veh_insurance_btn']         = 'Forsikrings handlinger',
  ['progbar_retrieve_veh']      = 'HENTER EJNE KoeREToeJER',
  ['shop_sell_title']      		= 'PDM BIL SHOP',
  ['shop_sell_msg']      		= 'Du modtog $%s for at saelge din bil',
  ['shop_sell_veh_taken']      	= 'PDM har taget dit koeretoej med ind paa deres lager',
  ['shop_sell_confirm']      	= 'Bekraeft koeretoejssalg?',
  ['progbar_retrieve_bills']    = 'HENTER FINANSIERINGS REGNSKABER',
  ['shop_finance_title']      	= 'PDM-finansierings regninger',
  ['repay_dialog_title']      	= 'Tilbagebetal beloeb [Min: $%s]',
  ['min_repayment']      		= 'Nuvaerende tilbagebetaling er mindst: $%s',
  ['paid_fin_bill']      		= 'Du betalte $%s til din finansering',
  ['shop_catalog']      		= 'PDM katalog',
  ['shop_veh_list']      		= 'PDM: %s',
  ['shop_veh_confirm']      	= 'Bekraeft koeb?',
  
  -- SELL CAR MENU:
  ['open_sell_menu']            = '~r~[E]~s~ FOR AT ~y~SaeLGE~s~ BILEN',
  ['not_able_to_sell']            = 'Du kan ikke saelge dette koeretoej',
  ['still_owe_money']            = 'Du skylder stadig penge paa dette koeretoej',

  -- CAR DEALER:
  ['veh_purchased']         	= 'Tak for dit koeb. Din nye bil er ankommet',
  ['veh_purchase_cancel']       = 'Du har annulleret koebet',
  ['veh_not_enough_money']      = 'Ikke nok penge',
  ['return_test_veh']      		= '~r~[E]~s~ AT VENDE TILBAGE FRA~y~TEST KoeRSEL~s~',
  ['test_veh_returned']      	= 'Du returnerede testkoeretoejet',
  ['test_veh_spawned']      	= 'Testkoeretoej er ankommet',
  ['already_test_driving']      = 'Testkoeretoej er allerede ankommet',
  ['veh_not_in_stock']      	= 'Denne model er udsolgt',
  ['veh_repossessed_warning']   = 'Du har ubetalte tilbagebetalinger, betal inden for ~b~%s minutter~s~, ellers vil de blive hentet af PDM',
  ['veh_repossessed']  			= 'Din bil med nr. pladen: ~b~%s~s~ Er blevet hentet af ~r~PDM~s~',
  ['plate_nil']  				= 'Den Skrevet Nr. plade er ikke gyldig...',
  
  -- DISPLAY VEHICLE:
  ['swap_car_label']         	= '~r~[G]~s~ Skift',
  ['confirm_cancel_purchase']   = '~g~[E]~s~ Underskriv koebs papir | ~r~[BACKSAPCE]~s~ For at annulere koebet',
  ['min_commission_limit']     	= 'Du naaede den mindste tilladte provision',
  ['max_commission_limit']     	= 'Du naaede den maksimalt tilladte provision',
  ['confirm_cancel_finance']    = '~g~[K]~s~ Underskriv finansieringspapirer | ~r~[BACKSAPCE]~s~ For at annulere',
  ['max_downpayment_limit']     = 'Du naaede det maksimalt tilladte afbetaling',
  ['min_downpayment_limit']     = 'Du naaede det tilladte minimum afbetaling',
    
  -- REGISTRATION PAPER:
  ['plate_not_exists']  		= 'Nr. pladen findes ikke',
  ['nobody_near']  				= 'Ingen spillere i naerheden',
  ['veh_owner_change']  		= 'Du gav registreringspapir til den naermeste spiller',
  ['veh_owner_change2']  		= 'Du modtog registreringspapir fra den naermeste spiller',
  ['reg_payment_label1']  		= 'Finansiering',
  ['reg_payment_label2']  		= 'Kontantafregning',
    
  -- OTHER:
  ['not_own_that_plate']     	= "Du ejer ikke et koeretoej med den plade",

}
