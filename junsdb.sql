-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for junsdb
CREATE DATABASE IF NOT EXISTS `junsdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `junsdb`;

-- Dumping structure for table junsdb.addon_account
CREATE TABLE IF NOT EXISTS `addon_account` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.addon_account: ~6 rows (approximately)
/*!40000 ALTER TABLE `addon_account` DISABLE KEYS */;
INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
	('caution', 'caution', 0),
	('society_ambulance', 'EMS', 1),
	('society_cardealer', 'Car Dealer', 1),
	('society_mechanic', 'Mechanic', 1),
	('society_police', 'Police', 1),
	('society_taxi', 'Taxi', 1);
/*!40000 ALTER TABLE `addon_account` ENABLE KEYS */;

-- Dumping structure for table junsdb.addon_account_data
CREATE TABLE IF NOT EXISTS `addon_account_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) DEFAULT NULL,
  `money` int(11) NOT NULL,
  `owner` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_addon_account_data_account_name_owner` (`account_name`,`owner`),
  KEY `index_addon_account_data_account_name` (`account_name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.addon_account_data: ~7 rows (approximately)
/*!40000 ALTER TABLE `addon_account_data` DISABLE KEYS */;
INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
	(2, 'society_police', 0, NULL),
	(3, 'society_ambulance', 0, NULL),
	(4, 'society_mechanic', 0, NULL),
	(5, 'society_taxi', 0, NULL),
	(8, 'caution', 0, 'Char1:11000010c863ac5'),
	(9, 'caution', 0, 'steam:11000010c863ac5'),
	(12, 'society_cardealer', 533040, NULL);
/*!40000 ALTER TABLE `addon_account_data` ENABLE KEYS */;

-- Dumping structure for table junsdb.addon_inventory
CREATE TABLE IF NOT EXISTS `addon_inventory` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.addon_inventory: ~5 rows (approximately)
/*!40000 ALTER TABLE `addon_inventory` DISABLE KEYS */;
INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
	('society_ambulance', 'EMS', 1),
	('society_cardealer', 'Car Dealer', 1),
	('society_mechanic', 'Mechanic', 1),
	('society_police', 'Police', 1),
	('society_taxi', 'Taxi', 1);
/*!40000 ALTER TABLE `addon_inventory` ENABLE KEYS */;

-- Dumping structure for table junsdb.addon_inventory_items
CREATE TABLE IF NOT EXISTS `addon_inventory_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_name` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_addon_inventory_items_inventory_name_name` (`inventory_name`,`name`),
  KEY `index_addon_inventory_items_inventory_name_name_owner` (`inventory_name`,`name`,`owner`),
  KEY `index_addon_inventory_inventory_name` (`inventory_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.addon_inventory_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `addon_inventory_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `addon_inventory_items` ENABLE KEYS */;

-- Dumping structure for table junsdb.allhousing
CREATE TABLE IF NOT EXISTS `allhousing` (
  `id` int(11) NOT NULL,
  `owner` varchar(50) NOT NULL,
  `ownername` varchar(50) NOT NULL,
  `owned` tinyint(4) NOT NULL,
  `price` int(11) NOT NULL,
  `resalepercent` int(11) NOT NULL,
  `resalejob` varchar(50) NOT NULL,
  `entry` longtext DEFAULT NULL,
  `garage` longtext DEFAULT NULL,
  `furniture` longtext DEFAULT NULL,
  `shell` varchar(50) NOT NULL,
  `interior` varchar(50) NOT NULL,
  `shells` longtext DEFAULT NULL,
  `doors` longtext DEFAULT NULL,
  `housekeys` longtext DEFAULT NULL,
  `wardrobe` longtext DEFAULT NULL,
  `inventory` longtext DEFAULT NULL,
  `inventorylocation` longtext DEFAULT NULL,
  `mortgage_owed` int(11) NOT NULL DEFAULT 0,
  `last_repayment` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table junsdb.allhousing: ~28 rows (approximately)
/*!40000 ALTER TABLE `allhousing` DISABLE KEYS */;
INSERT INTO `allhousing` (`id`, `owner`, `ownername`, `owned`, `price`, `resalepercent`, `resalejob`, `entry`, `garage`, `furniture`, `shell`, `interior`, `shells`, `doors`, `housekeys`, `wardrobe`, `inventory`, `inventorylocation`, `mortgage_owed`, `last_repayment`) VALUES
	(1, '', '', 0, 25000, 0, '', '{"z":23.0,"w":200.0,"x":54.25,"y":-1873.3399658203126}', '{"z":22.5,"w":45.0,"x":58.77000045776367,"y":-1881.72998046875}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(2, '', '', 0, 25000, 0, '', '{"z":23.5,"w":200.0,"x":45.72999954223633,"y":-1864.5}', '{"z":22.82999992370605,"w":135.0,"x":42.1500015258789,"y":-1852.8199462890626}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(3, '', '', 0, 25000, 0, '', '{"z":24.20000076293945,"w":200.0,"x":29.85000038146972,"y":-1854.449951171875}', '[]', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(4, '', '', 0, 25000, 0, '', '{"z":25.0,"w":200.0,"x":21.14999961853027,"y":-1844.3199462890626}', '{"z":24.29999923706054,"w":135.0,"x":10.06999969482421,"y":-1845.3499755859376}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(5, '', '', 0, 25000, 0, '', '{"z":24.0,"w":200.0,"x":5.09000015258789,"y":-1884.22998046875}', '{"z":23.14999961853027,"w":319.0,"x":15.1899995803833,"y":-1883.3699951171876}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(6, '', '', 0, 25000, 0, '', '{"z":26.5,"w":200.0,"x":-34.13000106811523,"y":-1847.199951171875}', '[]', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(7, '', '', 0, 25000, 0, '', '{"z":25.60000038146972,"w":200.0,"x":-20.51000022888183,"y":-1858.719970703125}', '{"z":25.09000015258789,"w":25.09000015258789,"x":-22.97999954223632,"y":-1852.4300537109376}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(8, '', '', 0, 25000, 0, '', '{"z":24.5,"w":200.0,"x":-5.01999998092651,"y":-1872.18994140625}', '{"z":23.64999961853027,"w":315.5,"x":-4.86999988555908,"y":-1883.2900390625}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(9, '', '', 0, 25000, 0, '', '{"z":23.29999923706054,"w":200.0,"x":23.23999977111816,"y":-1896.550048828125}', '{"z":22.17000007629394,"w":316.6499938964844,"x":18.02000045776367,"y":-1885.3499755859376}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(10, '', '', 0, 25000, 0, '', '{"z":22.29999923706054,"w":200.0,"x":39.11000061035156,"y":-1911.5899658203126}', '{"z":21.67000007629394,"w":316.6499938964844,"x":39.27000045776367,"y":-1924.1099853515626}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(11, '', '', 0, 25000, 0, '', '{"z":21.90999984741211,"w":200.0,"x":56.5099983215332,"y":-1922.6600341796876}', '{"z":21.32999992370605,"w":319.95001220703127,"x":68.0,"y":-1921.8599853515626}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(12, '', '', 0, 25000, 0, '', '{"z":21.3700008392334,"w":200.0,"x":72.18000030517578,"y":-1939.0899658203126}', '[]', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(13, '', '', 0, 25000, 0, '', '{"z":21.17000007629394,"w":200.0,"x":76.20999908447266,"y":-1948.1400146484376}', '[]', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(14, '', '', 0, 25000, 0, '', '{"z":21.1200008392334,"w":200.0,"x":85.88999938964844,"y":-1959.6800537109376}', '{"z":20.75,"w":316.6499938964844,"x":85.61000061035156,"y":-1971.300048828125}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(15, '', '', 0, 25000, 0, '', '{"z":21.32999992370605,"w":200.0,"x":114.19000244140625,"y":-1961.1099853515626}', '{"z":20.75,"w":0.94999998807907,"x":103.76000213623047,"y":-1957.2900390625}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(16, '', '', 0, 25000, 0, '', '{"z":21.3799991607666,"w":200.0,"x":126.68000030517578,"y":-1930.06005859375}', '{"z":20.65999984741211,"w":111.95999908447266,"x":127.58999633789063,"y":-1939.4300537109376}', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(17, '', '', 0, 25000, 0, '', '{"z":21.31999969482422,"w":200.0,"x":118.41999816894531,"y":-1921.1199951171876}', '[]', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(18, '', '', 0, 25000, 0, '', '{"z":21.40999984741211,"w":200.0,"x":100.9000015258789,"y":-1912.18994140625}', '[]', '[]', 'HotelV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(19, '', '', 0, 55000, 0, '', '{"z":71.7300033569336,"w":160.6300048828125,"x":1301.0899658203126,"y":-574.5599975585938}', '{"z":71.73999786376953,"w":343.0,"x":1291.0899658203126,"y":-583.010009765625}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(20, '', '', 0, 55000, 0, '', '{"z":73.23999786376953,"w":0.15000000596046,"x":1323.3900146484376,"y":-582.9600219726563}', '{"z":72.93000030517578,"w":340.0,"x":1312.969970703125,"y":-588.8599853515625}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(21, '', '', 0, 55000, 0, '', '{"z":74.69999694824219,"w":220.3000030517578,"x":1341.3699951171876,"y":-597.1900024414063}', '{"z":74.3499984741211,"w":323.0,"x":1346.8599853515626,"y":-606.7000122070313}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(22, '', '', 0, 55000, 0, '', '{"z":74.70999908447266,"w":0.75999999046325,"x":1367.219970703125,"y":-606.47998046875}', '{"z":74.33000183105469,"w":360.0,"x":1360.2099609375,"y":-615.8400268554688}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(23, '', '', 0, 55000, 0, '', '{"z":74.4800033569336,"w":75.66000366210938,"x":1386.050048828125,"y":-593.4099731445313}', '{"z":74.33000183105469,"w":55.20999908447265,"x":1389.989990234375,"y":-605.3200073242188}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(24, '', '', 0, 55000, 0, '', '{"z":74.48999786376953,"w":135.97999572753907,"x":1388.9599609375,"y":-569.6099853515625}', '{"z":74.33000183105469,"w":115.19999694824219,"x":1400.969970703125,"y":-572.2000122070313}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(25, '', '', 0, 55000, 0, '', '{"z":74.68000030517578,"w":90.43000030517578,"x":1373.260009765625,"y":-555.8400268554688}', '{"z":74.33000183105469,"w":155.9499969482422,"x":1365.3900146484376,"y":-547.7999877929688}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(26, '', '', 0, 55000, 0, '', '{"z":73.88999938964844,"w":170.16000366210938,"x":1348.3399658203126,"y":-546.9000244140625}', '{"z":73.7699966430664,"w":160.61000061035157,"x":1358.3299560546876,"y":-541.3599853515625}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(27, '', '', 0, 55000, 0, '', '{"z":72.44000244140625,"w":90.20999908447266,"x":1328.5,"y":-536.0}', '{"z":72.12000274658203,"w":159.91000366210938,"x":1320.4100341796876,"y":-528.3300170898438}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0),
	(28, '', '', 0, 55000, 0, '', '{"z":71.45999908447266,"w":180.55999755859376,"x":1303.199951171875,"y":-527.4600219726563}', '{"z":71.30999755859375,"w":162.44000244140626,"x":1312.6600341796876,"y":-521.6900024414063}', '[]', 'ApartmentV1', '', '{"ApartmentV1":true,"HotelV1":true}', NULL, '[]', '[]', '{"Cash":0,"Weapons":[],"DirtyMoney":0,"Items":[]}', '[]', 0, 0);
/*!40000 ALTER TABLE `allhousing` ENABLE KEYS */;

-- Dumping structure for table junsdb.billing
CREATE TABLE IF NOT EXISTS `billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(40) NOT NULL,
  `sender` varchar(40) NOT NULL,
  `target_type` varchar(50) NOT NULL,
  `target` varchar(40) NOT NULL,
  `label` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.billing: ~0 rows (approximately)
/*!40000 ALTER TABLE `billing` DISABLE KEYS */;
/*!40000 ALTER TABLE `billing` ENABLE KEYS */;

-- Dumping structure for table junsdb.cardealer_vehicles
CREATE TABLE IF NOT EXISTS `cardealer_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.cardealer_vehicles: ~0 rows (approximately)
/*!40000 ALTER TABLE `cardealer_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `cardealer_vehicles` ENABLE KEYS */;

-- Dumping structure for table junsdb.cyber_inventory
CREATE TABLE IF NOT EXISTS `cyber_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` text COLLATE utf8_turkish_ci NOT NULL,
  `type` text COLLATE utf8_turkish_ci DEFAULT NULL,
  `data` longtext COLLATE utf8_turkish_ci NOT NULL,
  `slot` int(11) NOT NULL DEFAULT 1,
  `drop` text COLLATE utf8_turkish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- Dumping data for table junsdb.cyber_inventory: ~1 rows (approximately)
/*!40000 ALTER TABLE `cyber_inventory` DISABLE KEYS */;
INSERT INTO `cyber_inventory` (`id`, `owner`, `type`, `data`, `slot`, `drop`) VALUES
	(2, 'steam:11000010c863ac5', 'player', '{"3":{"quality":83.99999999999977,"usable":false,"grade":0,"count":1,"price":1060,"name":"WEAPON_CARBINERIFLE"},"4":{"quality":100,"usable":false,"grade":0,"count":1,"price":100,"name":"sight"}}', 1, NULL);
/*!40000 ALTER TABLE `cyber_inventory` ENABLE KEYS */;

-- Dumping structure for table junsdb.cyber_itemdata
CREATE TABLE IF NOT EXISTS `cyber_itemdata` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_turkish_ci NOT NULL,
  `description` text COLLATE utf8_turkish_ci DEFAULT NULL,
  `max` int(11) NOT NULL DEFAULT 100,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

-- Dumping data for table junsdb.cyber_itemdata: ~108 rows (approximately)
/*!40000 ALTER TABLE `cyber_itemdata` DISABLE KEYS */;
INSERT INTO `cyber_itemdata` (`id`, `name`, `description`, `max`) VALUES
	(1, 'cash', 'Cash money, spend it however you want.', 2147483647),
	(2, 'WEAPON_ADVANCEDRIFLE', 'Advanced rifle, very useful for long distances.', 1),
	(3, 'WEAPON_APPISTOL', 'The AP pistol is an automatic machine gun that acts like a 9mm pistol.', 1),
	(4, 'WEAPON_ASSAULTRIFLE', 'An assault rifle is a rifle designed for combat, selective fire.', 1),
	(5, 'WEAPON_ASSAULTSHOTGUN', 'A very powerful assault shotgun, it differs from the rest by its cylindrical magazine.', 1),
	(6, 'WEAPON_ASSAULTSMG', 'Short and medium distance weapon, very powerful and with a fast burst.', 1),
	(7, 'WEAPON_AUTOSHOTGUN', 'Automatic shotgun, very useful.', 1),
	(8, 'WEAPON_BALL', 'A pet could play with this, it looks like fun.', 1),
	(9, 'WEAPON_BAT', 'A simple baseball bat, can you think of what to use it with?', 1),
	(10, 'WEAPON_BATTLEAXE', 'Battle ax, a bit old-fashioned...', 1),
	(11, 'WEAPON_BOTTLE', 'A bottle of Nuka-Cola, reminds me of another video game...', 1),
	(12, 'WEAPON_BULLPUPRIFLE', 'The primary benefit of a bullpup weapon is that the weapons overall length can be significantly decreased without reducing the barrel length.', 1),
	(13, 'WEAPON_BULLPUPSHOTGUN', 'The term bullpup refers to a firearm format, in which the mechanisms and the magazine are located behind the trigger.', 1),
	(14, 'WEAPON_BZGAS', 'It could be used for a police raid, take care that there are no children around.', 1),
	(15, 'WEAPON_CARBINERIFLE', 'Automatic tactical rifle, super powerful burst, try it with some accessories.', 1),
	(16, 'WEAPON_COMBATMG', 'Be careful with that, it is heavy and very powerful.', 1),
	(17, 'WEAPON_COMBATPDW', 'It looks futuristic, but it is a very good weapon.', 1),
	(18, 'WEAPON_COMBATPISTOL', 'Very good combat pistol to have a discreet weapon in your pockets.', 1),
	(19, 'WEAPON_COMPACTLAUNCHER', 'A pet could play with this, it looks like fun.', 1),
	(20, 'WEAPON_COMPACTLAUNCHER', 'Sawn-off shotgun, looks homemade, how will it work?', 1),
	(21, 'WEAPON_COMPACTRIFLE', 'Compact rifle, looks powerful.', 1),
	(22, 'WEAPON_CROWBAR', 'You can possibly force something with this.', 1),
	(23, 'WEAPON_DAGGER', 'A simple dagger, it seems quite sharp, be careful.', 1),
	(24, 'WEAPON_DBSHOTGUN', 'Double-barreled shotgun, short range, but plenty of power, believe me.', 1),
	(25, 'WEAPON_DIGISCANNER', 'What is this supposed to be?', 1),
	(26, 'WEAPON_DOUBLEACTION', 'Wow, it looks like its gold plated.', 1),
	(27, 'WEAPON_FIREEXTINGUISHER', 'Be careful, do not extinguish the flames of his heart.', 1),
	(28, 'WEAPON_FIREWORK', 'Hey hey... watch that bro.', 1),
	(29, 'WEAPON_FLARE', 'I dont know what this is, but it looks nice, doesnt it?', 1),
	(30, 'WEAPON_FLAREGUN', 'If you get lost at sea, you can use it to be rescued.', 1),
	(31, 'WEAPON_FLASHLIGHT', 'Be careful, you can make someone blind.', 1),
	(32, 'WEAPON_GARBAGEBAG', 'It smells bad, please drop it.', 1),
	(33, 'WEAPON_GOLFCLUB', 'It seems to be signed by a famous person, keep it well, it looks beautiful.', 1),
	(34, 'WEAPON_GRENADE', 'Keep that to yourself or do you want to kill us all?', 1),
	(35, 'WEAPON_GRENADELAUNCHER', 'Where did you get that? Its very dangerous brother.', 1),
	(36, 'WEAPON_GUSENBERG', 'I think I saw an equal in Scarface, very cool.', 1),
	(37, 'WEAPON_HAMMER', 'It seems to be very worn.', 1),
	(38, 'WEAPON_HANDCUFFS', 'Keep the key, if not you will not be able to release your prisoner.', 1),
	(39, 'WEAPON_HATCHET', 'The trees will tremble before you.', 1),
	(40, 'WEAPON_HEAVYPISTOL', 'Worthy of a good thief.', 1),
	(41, 'WEAPON_HEAVYSHOTGUN', 'Very good weapon, you should be careful when you use it, it looks very aggressive.', 1),
	(42, 'WEAPON_HEAVYSNIPER', 'One shot, one death.', 1),
	(43, 'WEAPON_HOMINGLAUNCHER', 'Ok, this got serious...', 1),
	(44, 'WEAPON_KNIFE', 'I dont know if you want to peel your grandfathers oranges or kill a person...', 1),
	(45, 'WEAPON_KNUCKLE', 'Cute brass knuckles, you should feel like One Punch Man.', 1),
	(46, 'WEAPON_MACHETE', 'You could use to butcher someone, or simply to cut the meat from your grill.', 1),
	(47, 'WEAPON_MACHINEPISTOL', 'Super powerful burst pistol.', 1),
	(48, 'WEAPON_MARKSMANPISTOL', 'You look like Jack Sparrow haha.', 1),
	(49, 'WEAPON_MARKSMANRIFLE', 'A lightweight sniper, very comfortable indeed.', 1),
	(50, 'WEAPON_MG', 'What kind of mess can we make with this?', 1),
	(51, 'WEAPON_MICROSMG', 'It looks small but has such a fast burst, youll be amazed when you try it.', 1),
	(52, 'WEAPON_MINIGUN', 'Classic minigun, you could use on a motorcycle or a car.', 1),
	(53, 'WEAPON_MINISMG', 'You could use on a motorcycle or a car.', 1),
	(54, 'WEAPON_MOLOTOV', 'Were going to burn down the whole city with this.', 1),
	(55, 'WEAPON_MUSKET', 'A relic of the world war.', 1),
	(56, 'WEAPON_NIGHTSTICK', 'Stop there vermin, authority is coming.', 1),
	(57, 'WEAPON_PETROLCAN', 'A very powerful smelly bottle.', 1),
	(58, 'WEAPON_PIPEBOMB', 'We could rob a bank with this.', 1),
	(59, 'WEAPON_PISTOL', 'Simple pistol, it is simple but very good for short fights or just to defend yourself.', 1),
	(60, 'WEAPON_PISTOL50', 'Simple pistol, it is simple but very good for short fights or just to defend yourself.', 1),
	(61, 'WEAPON_POOLCUE', 'Are you going to play pool?', 1),
	(62, 'WEAPON_PROXMINE', 'Proximity bomb, wouldnt you like to rob a bank with it?', 1),
	(63, 'WEAPON_PUMPSHOTGUN', 'Classic shotgun pump, it sure sounds like Fortnite haha.', 1),
	(64, 'WEAPON_RAILGUN', 'Wont you be the new admin?', 1),
	(65, 'WEAPON_REVOLVER', 'Drum revolver, like the wild west.', 1),
	(66, 'WEAPON_RPG', 'It is definitely not kid friendly.', 1),
	(67, 'WEAPON_SAWNOFFSHOTGUN', 'Police shotgun, very powerful indeed.', 1),
	(68, 'WEAPON_SMOKEGRENADE', 'Smoke grenade, if you were a magician, you would look great haha.', 1),
	(69, 'WEAPON_SNIPERRIFLE', 'A powerful sniper rifle, one shot, one kill.', 1),
	(70, 'WEAPON_SNOWBALL', 'Merry Christmas!', 50),
	(71, 'WEAPON_SNSPISTOL', 'Powerful pistol, be careful with it.', 1),
	(72, 'WEAPON_SPECIALCARBINE', 'Special carbine, mostly used by police or military.', 1),
	(73, 'WEAPON_STICKYBOMB', 'Are you going to rob a bank, or what?', 1),
	(74, 'WEAPON_STINGER', 'Hey hey, be careful brother.', 1),
	(75, 'WEAPON_STUNGUN', 'Stun gun, it doesnt do much damage, but I assure you it will hurt.', 1),
	(76, 'WEAPON_SWITCHBLADE', 'A pet could play with this, it looks like fun.', 1),
	(77, 'WEAPON_SWITCHBLADE', 'You look like a grandmothers thief, get yourself a real gun.', 1),
	(78, 'WEAPON_VINTAGEPISTOL', 'Old pistol, it belonged to Hitlers cousin, it has a lot of history.', 1),
	(79, 'WEAPON_WRENCH', 'Mechanics use it a lot.', 50),
	(80, 'cigarett', 'Smoking is bad, be careful, your lungs are having a hard time.', 50),
	(81, 'beer', 'A good beer is not denied to anyone, that you enjoy it.', 50),
	(82, 'lighter', 'A cute Zippo brand lighter.', 50),
	(83, 'chips', 'French fries, succulent.', 50),
	(84, 'chocolate', 'A rich dark chocolate.', 50),
	(85, 'cupcake', 'Who does not want a cupcake?', 50),
	(86, 'coffee', 'A delicious coffee.', 50),
	(87, 'milk', 'Milk is good for your bones, drink it.', 50),
	(88, 'cocacola', 'A good refreshment for a hot day.', 50),
	(89, 'phone', 'Cyber ​​brand phone.', 50),
	(90, 'bread', 'It looks rich and healthy.', 50),
	(91, 'water', 'There is nothing more refreshing than a bottle of water.', 1),
	(92, 'bag', 'Puedes equiparte usando /bagon.', 1),
	(93, 'cyber_ammo_pistol', 'Bullet cartridge.', 50),
	(94, 'cyber_ammo_pistol_large', 'Bullet cartridge.', 50),
	(95, 'cyber_ammo_rifle', 'Bullet cartridge.', 50),
	(96, 'cyber_ammo_rifle_large', 'Bullet cartridge.', 50),
	(97, 'cyber_ammo_shotgun', 'Bullet cartridge.', 50),
	(98, 'cyber_ammo_shotgun_large', 'Bullet cartridge.', 50),
	(99, 'cyber_ammo_smg', 'Bullet cartridge.', 50),
	(100, 'cyber_ammo_smg_large', 'Bullet cartridge.', 50),
	(101, 'cyber_ammo_snp', 'Bullet cartridge.', 50),
	(102, 'extended', 'Component for weapons.', 50),
	(103, 'light', 'Component for weapons.', 50),
	(104, 'sight', 'Component for weapons.', 50),
	(105, 'grip', 'Component for weapons.', 50),
	(106, 'goldtint', 'Component for weapons.', 50),
	(107, 'suppressor', 'Component for weapons.', 50),
	(108, 'bulletproof', 'It could save you when you least expect it.', 50);
/*!40000 ALTER TABLE `cyber_itemdata` ENABLE KEYS */;

-- Dumping structure for table junsdb.cyber_weapons
CREATE TABLE IF NOT EXISTS `cyber_weapons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `owner` text NOT NULL,
  `hash` text NOT NULL,
  `ammo` text NOT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `suppressor` int(11) DEFAULT 0,
  `light` int(11) DEFAULT 0,
  `grip` int(11) DEFAULT 0,
  `goldtint` int(11) DEFAULT 0,
  `sight` int(11) DEFAULT 0,
  `extended` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table junsdb.cyber_weapons: ~2 rows (approximately)
/*!40000 ALTER TABLE `cyber_weapons` DISABLE KEYS */;
INSERT INTO `cyber_weapons` (`id`, `owner`, `hash`, `ammo`, `count`, `suppressor`, `light`, `grip`, `goldtint`, `sight`, `extended`) VALUES
	(2, 'steam:11000010c863ac5', '', '{"AMMO_RIFLE":1}', 0, 0, 0, 0, 0, 0, 0),
	(3, 'steam:11000010c863ac5', '-2084633992', '{"AMMO_RIFLE":1}', 0, 0, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `cyber_weapons` ENABLE KEYS */;

-- Dumping structure for table junsdb.datastore
CREATE TABLE IF NOT EXISTS `datastore` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.datastore: ~8 rows (approximately)
/*!40000 ALTER TABLE `datastore` DISABLE KEYS */;
INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
	('society_ambulance', 'EMS', 1),
	('society_mechanic', 'Mechanic', 1),
	('society_police', 'Police', 1),
	('society_taxi', 'Taxi', 1),
	('user_ears', 'Ears', 0),
	('user_glasses', 'Glasses', 0),
	('user_helmet', 'Helmet', 0),
	('user_mask', 'Mask', 0);
/*!40000 ALTER TABLE `datastore` ENABLE KEYS */;

-- Dumping structure for table junsdb.datastore_data
CREATE TABLE IF NOT EXISTS `datastore_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `owner` varchar(40) DEFAULT NULL,
  `data` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_datastore_data_name_owner` (`name`,`owner`),
  KEY `index_datastore_data_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.datastore_data: ~8 rows (approximately)
/*!40000 ALTER TABLE `datastore_data` DISABLE KEYS */;
INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
	(1, 'society_police', NULL, '{}'),
	(2, 'society_ambulance', NULL, '{}'),
	(3, 'society_mechanic', NULL, '{}'),
	(4, 'society_taxi', NULL, '{}'),
	(5, 'user_ears', 'steam:11000010c863ac5', '{}'),
	(6, 'user_glasses', 'steam:11000010c863ac5', '{}'),
	(7, 'user_mask', 'steam:11000010c863ac5', '{}'),
	(8, 'user_helmet', 'steam:11000010c863ac5', '{}');
/*!40000 ALTER TABLE `datastore_data` ENABLE KEYS */;

-- Dumping structure for table junsdb.dopeplants
CREATE TABLE IF NOT EXISTS `dopeplants` (
  `owner` varchar(50) NOT NULL,
  `plant` longtext NOT NULL,
  `plantid` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.dopeplants: ~0 rows (approximately)
/*!40000 ALTER TABLE `dopeplants` DISABLE KEYS */;
/*!40000 ALTER TABLE `dopeplants` ENABLE KEYS */;

-- Dumping structure for table junsdb.dpkeybinds
CREATE TABLE IF NOT EXISTS `dpkeybinds` (
  `id` varchar(50) DEFAULT NULL,
  `keybind1` varchar(50) DEFAULT 'num4',
  `emote1` varchar(255) DEFAULT '',
  `keybind2` varchar(50) DEFAULT 'num5',
  `emote2` varchar(255) DEFAULT '',
  `keybind3` varchar(50) DEFAULT 'num6',
  `emote3` varchar(255) DEFAULT '',
  `keybind4` varchar(50) DEFAULT 'num7',
  `emote4` varchar(255) DEFAULT '',
  `keybind5` varchar(50) DEFAULT 'num8',
  `emote5` varchar(255) DEFAULT '',
  `keybind6` varchar(50) DEFAULT 'num9',
  `emote6` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.dpkeybinds: ~1 rows (approximately)
/*!40000 ALTER TABLE `dpkeybinds` DISABLE KEYS */;
INSERT INTO `dpkeybinds` (`id`, `keybind1`, `emote1`, `keybind2`, `emote2`, `keybind3`, `emote3`, `keybind4`, `emote4`, `keybind5`, `emote5`, `keybind6`, `emote6`) VALUES
	('steam:11000010c863ac5', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');
/*!40000 ALTER TABLE `dpkeybinds` ENABLE KEYS */;

-- Dumping structure for table junsdb.items
CREATE TABLE IF NOT EXISTS `items` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `limit` int(11) NOT NULL DEFAULT -1,
  `rare` int(11) NOT NULL DEFAULT 0,
  `can_remove` int(11) NOT NULL DEFAULT 1,
  `weight` float(4,2) NOT NULL DEFAULT 0.00,
  `max` int(11) DEFAULT NULL,
  `durability` int(6) NOT NULL DEFAULT 100,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Dumping data for table junsdb.items: ~165 rows (approximately)
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`, `weight`, `max`, `durability`) VALUES
	('accesscard', 'Access Card', 10, 0, 1, 0.00, NULL, 100),
	('aluminium', 'Aluminium', 50, 0, 1, 0.00, NULL, 100),
	('aluminum', 'Aluminum', 10, 0, 1, 0.00, NULL, 100),
	('ammoanalyzer', 'Ammo Analyzer', 1, 0, 1, 0.00, NULL, 100),
	('armbrace', 'Arm Brace', 5, 0, 1, 0.00, NULL, 100),
	('armor', 'Armor', 2, 0, 1, 0.00, NULL, 100),
	('bagofdope', 'Bag of Dope', -1, 0, 1, 0.00, NULL, 100),
	('bankidcard', 'Bank ID', -1, 0, 1, 0.00, NULL, 100),
	('beer', 'Beer', -1, 0, 1, 0.50, NULL, 100),
	('binoculars', 'Binoculars', 1, 0, 1, 0.00, NULL, 100),
	('bloodsample', 'Blood Sample', 1, 0, 1, 0.00, NULL, 100),
	('bodybandage', 'Body Bandage', 5, 0, 1, 0.00, NULL, 100),
	('bread', 'Sandwich', -1, 0, 1, 0.50, NULL, 100),
	('bulletproof', 'Bulletproof', -1, 0, 1, 1.00, NULL, 100),
	('bulletsample', 'Bullet Casing', 1, 0, 1, 0.00, NULL, 100),
	('cash', 'Cash', -1, 0, 1, 0.00, NULL, 100),
	('chips', 'Chips', -1, 0, 1, 0.50, NULL, 100),
	('chocolate', 'Chocolate', -1, 0, 1, 0.50, NULL, 100),
	('cigarett', 'Cigarette', -1, 0, 1, 0.50, NULL, 100),
	('cigarette', 'Cigarette', -1, 0, 1, 0.00, NULL, 100),
	('cocacola', 'Coca-cola', -1, 0, 1, 0.50, NULL, 100),
	('coffee', 'Coffee', -1, 0, 1, 0.50, NULL, 100),
	('coke', 'Bag of coke', -1, 0, 1, 1.00, NULL, 100),
	('cokebrick', 'Brick of coke', -1, 0, 1, 1.00, NULL, 100),
	('copper', 'Copper', 50, 0, 1, 0.00, NULL, 100),
	('cupcake', 'Cupcake', -1, 0, 1, 0.50, NULL, 100),
	('cyber_ammo_pistol', 'Pistol Ammo', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_pistol_large', 'Pistol Ammo Large', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_rifle', 'Rifle Ammo', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_rifle_large', 'Rifle Ammo Large', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_shotgun', 'Shotgun Shells', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_shotgun_large', 'Shotgun Shells Large', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_smg', 'SMG Ammo', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_smg_large', 'SMG Ammo Large', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_snp', 'Sniper Ammo', -1, 0, 1, 0.30, NULL, 100),
	('cyber_ammo_snp_large', 'Sniper Ammo Large', -1, 0, 1, 0.30, NULL, 100),
	('diamond', 'Diamond', 10, 0, 1, 0.00, NULL, 100),
	('dnaanalyzer', 'DNA Analyzer', 1, 0, 1, 0.00, NULL, 100),
	('donut', 'Donut', 10, 0, 1, 0.00, NULL, 100),
	('dopebag', 'Ziplock Bag', -1, 0, 1, 0.00, NULL, 100),
	('drill', 'Drill', 15, 0, 1, 0.00, NULL, 100),
	('drugscales', 'Drug Scales', -1, 0, 1, 0.00, NULL, 100),
	('electric_scrap', 'Electric Scrap', 50, 0, 1, 0.00, NULL, 100),
	('extended', 'Extended', -1, 0, 1, 0.50, NULL, 100),
	('glass', 'Glass', 15, 0, 1, 0.00, NULL, 100),
	('goldbar', 'Gold Bar', 100, 0, 1, 0.00, NULL, 100),
	('goldnecklace', 'Gold Necklace', 150, 0, 1, 0.00, NULL, 100),
	('goldtint', 'Gun dye', -1, 0, 1, 0.50, NULL, 100),
	('goldwatch', 'Gold Watch', 200, 0, 1, 0.00, NULL, 100),
	('grip', 'Grip', -1, 0, 1, 0.50, NULL, 100),
	('hackerDevice', 'Hacker Device', 10, 0, 1, 0.00, NULL, 100),
	('hammerwirecutter', 'Hammer And Wire Cutter', 10, 0, 1, 0.00, NULL, 100),
	('highgradefemaleseed', 'Female Dope Seed+', -1, 0, 1, 0.00, NULL, 100),
	('highgradefert', 'High-Grade Fertilizer', -1, 0, 1, 0.00, NULL, 100),
	('highgrademaleseed', 'Male Dope Seed+', -1, 0, 1, 0.00, NULL, 100),
	('joint', 'Joint', -1, 0, 1, 0.00, NULL, 100),
	('legbrace', 'Leg Brace', 5, 0, 1, 0.00, NULL, 100),
	('light', 'Lantern', -1, 0, 1, 0.50, NULL, 100),
	('lighter', 'Lighter', -1, 0, 1, 0.50, NULL, 100),
	('lockpick', 'Lockpick', 1, 0, 1, 0.00, NULL, 100),
	('lowgradefemaleseed', 'Female Dope Seed', -1, 0, 1, 0.00, NULL, 100),
	('lowgradefert', 'Low-Grade Fertilizer', -1, 0, 1, 0.00, NULL, 100),
	('lowgrademaleseed', 'Male Dope Seed', -1, 0, 1, 0.00, NULL, 100),
	('metalscrap', 'Scrap Metal', 10, 0, 1, 0.00, NULL, 100),
	('milk', 'Milk', -1, 0, 1, 0.50, NULL, 100),
	('neckbrace', 'Neck Brace', 5, 0, 1, 0.00, NULL, 100),
	('oxycutter', 'Plasma Torch', -1, 0, 1, 0.00, NULL, 100),
	('oxygenmask', 'Oxygen Mask', 10, 0, 1, 0.00, NULL, 100),
	('Phone', 'Cyber Phone', -1, 0, 1, 0.50, NULL, 100),
	('pisswasser', 'Pisswasser', 10, 0, 1, 0.00, NULL, 100),
	('plantpot', 'Plant Pot', -1, 0, 1, 0.00, NULL, 100),
	('plastic', 'Plastic', 15, 0, 1, 0.00, NULL, 100),
	('purifiedwater', 'Purified Water', -1, 0, 1, 0.00, NULL, 100),
	('redgull', 'Energy Drink', 10, 0, 1, 0.00, NULL, 100),
	('rolex', 'Rolex Watch', -1, 0, 1, 0.00, NULL, 100),
	('rubber', 'Rubber', 50, 0, 1, 0.00, NULL, 100),
	('sandwich', 'Sandwich', 10, 0, 1, 0.00, NULL, 100),
	('scrap_metal', 'Scrap Metal', 50, 0, 1, 0.00, NULL, 100),
	('sight', 'Sight', -1, 0, 1, 0.50, NULL, 100),
	('soda', 'Soda', 5, 0, 1, 0.00, NULL, 100),
	('steel', 'Steel', 10, 0, 1, 0.00, NULL, 100),
	('suppressor', 'Suppressor', -1, 0, 1, 0.50, NULL, 100),
	('tacos', 'Tacos', 10, 0, 1, 0.00, NULL, 100),
	('trimmedweed', 'Trimmed Weed', -1, 0, 1, 0.00, NULL, 100),
	('turtlebait', 'Turtle Bait', 8, 0, 1, 0.00, NULL, 100),
	('umbrella', 'Umbrella', 10, 0, 1, 0.00, NULL, 100),
	('water', 'Water', -1, 0, 1, 0.50, NULL, 100),
	('wateringcan', 'Watering Can', -1, 0, 1, 0.00, NULL, 100),
	('WEAPON_ADVANCEDRIFLE', 'Advanced Rifle', -1, 0, 1, 0.50, NULL, 100),
	('WEAPON_APPISTOL', 'AP Pistol', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_ASSAULTRIFLE', 'Assault Rifle', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_ASSAULTSHOTGUN', 'Assault Shotgun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_ASSAULTSMG', 'Assault SMG', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_AUTOSHOTGUN', 'Auto Shotgun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_BALL', 'Ball', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_BAT', 'Bat', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_BATTLEAXE', 'Battle Axe', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_BOTTLE', 'Bottle', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_BULLPUPRIFLE', 'Bullpup Rifle', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_BULLPUPSHOTGUN', 'Bullpup Shotgun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_BZGAS', 'BZ Gas', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_CARBINERIFLE', 'Carbine Rifle', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_COMBATMG', 'Combat MG', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_COMBATPDW', 'Combat PDW', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_COMBATPISTOL', 'Combat Pistol', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_COMPACTLAUNCHER', 'Compact Launcher', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_COMPACTRIFLE', 'Compact Rifle', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_CROWBAR', 'Crowbar', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_DAGGER', 'Dagger', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_DBSHOTGUN', 'Double Barrel Shotgun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_DIGISCANNER', 'Digiscanner', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_DOUBLEACTION', 'Revolver VIP', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_FIREEXTINGUISHER', 'Fire Extinguisher', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_FIREWORK', 'Firework Launcher', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_FLARE', 'Flare', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_FLAREGUN', 'Flare Gun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_FLASHLIGHT', 'Flashlight', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_GARBAGEBAG', 'Garbage Bag', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_GOLFCLUB', 'Golf Club', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_GRENADE', 'Grenade', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_GRENADELAUNCHER', 'Gernade Launcher', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_GUSENBERG', 'Gusenberg', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_HAMMER', 'Hammer', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_HANDCUFFS', 'Handcuffs', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_HATCHET', 'Hatchet', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_HEAVYPISTOL', 'Heavy Pistol', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_HEAVYSHOTGUN', 'Heavy Shotgun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_HEAVYSNIPER', 'Heavy Sniper', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_HOMINGLAUNCHER', 'Homing Launcher', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_KNIFE', 'Knife', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_KNUCKLE', 'Knuckle Dusters ', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MACHETE', 'Machete', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MACHINEPISTOL', 'Machine Pistol', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MARKSMANPISTOL', 'Marksman Pistol', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MARKSMANRIFLE', 'Marksman Rifle', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MG', 'MG', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MICROSMG', 'Micro SMG', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MINIGUN', 'Minigun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MINISMG', 'Mini SMG', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MOLOTOV', 'Molotov', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_MUSKET', 'Musket', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_NIGHTSTICK', 'Police Baton', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_PETROLCAN', 'Petrol Can', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_PIPEBOMB', 'Pipe Bomb', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_PISTOL', 'Pistol', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_PISTOL50', 'Police .50', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_POOLCUE', 'Pool Cue', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_PROXMINE', 'Proximity Mine', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_PUMPSHOTGUN', 'Pump Shotgun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_RAILGUN', 'Rail Gun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_REVOLVER', 'Revolver', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_RPG', 'RPG', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_SAWNOFFSHOTGUN', 'Sawn Off Shotgun', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_SMG', 'SMG', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_SMOKEGRENADE', 'Smoke Gernade', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_SNIPERRIFLE', 'Sniper Rifle', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_SNOWBALL', 'Snow Ball', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_SNSPISTOL', 'SNS Pistol', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_SPECIALCARBINE', 'Special Rifle', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_STICKYBOMB', 'Sticky Bombs', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_STINGER', 'Stinger', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_STUNGUN', 'Police Taser', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_SWITCHBLADE', 'Switch Blade', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_VINTAGEPISTOL', 'Vintage Pistol', -1, 0, 1, 0.70, NULL, 100),
	('WEAPON_WRENCH', 'Wrench', -1, 0, 1, 0.70, NULL, 100);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;

-- Dumping structure for table junsdb.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.jobs: ~15 rows (approximately)
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
	('ambulance', 'EMS', 1),
	('cardealer', 'Car Dealer', 0),
	('fisherman', 'Fisherman', 0),
	('fueler', 'Fueler', 1),
	('garbage', 'Garbage Job', 0),
	('lumberjack', 'Lumberjack', 0),
	('mechanic', 'Mechanic', 0),
	('miner', 'Miner', 0),
	('police', 'LSPD', 1),
	('reporter', 'Reporter', 0),
	('shop', 'Shop', 0),
	('slaughterer', 'Butcher', 0),
	('tailor', 'Tailor', 0),
	('taxi', 'Taxi', 0),
	('unemployed', 'Unemployed', 0);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- Dumping structure for table junsdb.job_grades
CREATE TABLE IF NOT EXISTS `job_grades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(50) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext NOT NULL,
  `skin_female` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.job_grades: ~35 rows (approximately)
/*!40000 ALTER TABLE `job_grades` DISABLE KEYS */;
INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
	(1, 'unemployed', 0, 'unemployed', 'Unemployed', 200, '{}', '{}'),
	(2, 'police', 0, 'recruit', 'Recrue', 20, '{}', '{}'),
	(3, 'police', 1, 'officer', 'Officier', 40, '{}', '{}'),
	(4, 'police', 2, 'sergeant', 'Sergent', 60, '{}', '{}'),
	(5, 'police', 3, 'lieutenant', 'Lieutenant', 85, '{}', '{}'),
	(6, 'police', 4, 'boss', 'Commandant', 100, '{}', '{}'),
	(15, 'lumberjack', 0, 'employee', 'Employee', 0, '{}', '{}'),
	(16, 'fisherman', 0, 'employee', 'Employee', 0, '{}', '{}'),
	(17, 'fueler', 0, 'employee', 'Employee', 0, '{}', '{}'),
	(18, 'reporter', 0, 'employee', 'Employee', 0, '{}', '{}'),
	(19, 'tailor', 0, 'employee', 'Employee', 0, '{"mask_1":0,"arms":1,"glasses_1":0,"hair_color_2":4,"makeup_1":0,"face":19,"glasses":0,"mask_2":0,"makeup_3":0,"skin":29,"helmet_2":0,"lipstick_4":0,"sex":0,"torso_1":24,"makeup_2":0,"bags_2":0,"chain_2":0,"ears_1":-1,"bags_1":0,"bproof_1":0,"shoes_2":0,"lipstick_2":0,"chain_1":0,"tshirt_1":0,"eyebrows_3":0,"pants_2":0,"beard_4":0,"torso_2":0,"beard_2":6,"ears_2":0,"hair_2":0,"shoes_1":36,"tshirt_2":0,"beard_3":0,"hair_1":2,"hair_color_1":0,"pants_1":48,"helmet_1":-1,"bproof_2":0,"eyebrows_4":0,"eyebrows_2":0,"decals_1":0,"age_2":0,"beard_1":5,"shoes":10,"lipstick_1":0,"eyebrows_1":0,"glasses_2":0,"makeup_4":0,"decals_2":0,"lipstick_3":0,"age_1":0}', '{"mask_1":0,"arms":5,"glasses_1":5,"hair_color_2":4,"makeup_1":0,"face":19,"glasses":0,"mask_2":0,"makeup_3":0,"skin":29,"helmet_2":0,"lipstick_4":0,"sex":1,"torso_1":52,"makeup_2":0,"bags_2":0,"chain_2":0,"ears_1":-1,"bags_1":0,"bproof_1":0,"shoes_2":1,"lipstick_2":0,"chain_1":0,"tshirt_1":23,"eyebrows_3":0,"pants_2":0,"beard_4":0,"torso_2":0,"beard_2":6,"ears_2":0,"hair_2":0,"shoes_1":42,"tshirt_2":4,"beard_3":0,"hair_1":2,"hair_color_1":0,"pants_1":36,"helmet_1":-1,"bproof_2":0,"eyebrows_4":0,"eyebrows_2":0,"decals_1":0,"age_2":0,"beard_1":5,"shoes":10,"lipstick_1":0,"eyebrows_1":0,"glasses_2":0,"makeup_4":0,"decals_2":0,"lipstick_3":0,"age_1":0}'),
	(20, 'miner', 0, 'employee', 'Employee', 0, '{"tshirt_2":1,"ears_1":8,"glasses_1":15,"torso_2":0,"ears_2":2,"glasses_2":3,"shoes_2":1,"pants_1":75,"shoes_1":51,"bags_1":0,"helmet_2":0,"pants_2":7,"torso_1":71,"tshirt_1":59,"arms":2,"bags_2":0,"helmet_1":0}', '{}'),
	(21, 'slaughterer', 0, 'employee', 'Employee', 0, '{"age_1":0,"glasses_2":0,"beard_1":5,"decals_2":0,"beard_4":0,"shoes_2":0,"tshirt_2":0,"lipstick_2":0,"hair_2":0,"arms":67,"pants_1":36,"skin":29,"eyebrows_2":0,"shoes":10,"helmet_1":-1,"lipstick_1":0,"helmet_2":0,"hair_color_1":0,"glasses":0,"makeup_4":0,"makeup_1":0,"hair_1":2,"bproof_1":0,"bags_1":0,"mask_1":0,"lipstick_3":0,"chain_1":0,"eyebrows_4":0,"sex":0,"torso_1":56,"beard_2":6,"shoes_1":12,"decals_1":0,"face":19,"lipstick_4":0,"tshirt_1":15,"mask_2":0,"age_2":0,"eyebrows_3":0,"chain_2":0,"glasses_1":0,"ears_1":-1,"bags_2":0,"ears_2":0,"torso_2":0,"bproof_2":0,"makeup_2":0,"eyebrows_1":0,"makeup_3":0,"pants_2":0,"beard_3":0,"hair_color_2":4}', '{"age_1":0,"glasses_2":0,"beard_1":5,"decals_2":0,"beard_4":0,"shoes_2":0,"tshirt_2":0,"lipstick_2":0,"hair_2":0,"arms":72,"pants_1":45,"skin":29,"eyebrows_2":0,"shoes":10,"helmet_1":-1,"lipstick_1":0,"helmet_2":0,"hair_color_1":0,"glasses":0,"makeup_4":0,"makeup_1":0,"hair_1":2,"bproof_1":0,"bags_1":0,"mask_1":0,"lipstick_3":0,"chain_1":0,"eyebrows_4":0,"sex":1,"torso_1":49,"beard_2":6,"shoes_1":24,"decals_1":0,"face":19,"lipstick_4":0,"tshirt_1":9,"mask_2":0,"age_2":0,"eyebrows_3":0,"chain_2":0,"glasses_1":5,"ears_1":-1,"bags_2":0,"ears_2":0,"torso_2":0,"bproof_2":0,"makeup_2":0,"eyebrows_1":0,"makeup_3":0,"pants_2":0,"beard_3":0,"hair_color_2":4}'),
	(22, 'ambulance', 0, 'ambulance', 'Jr. EMT', 20, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(23, 'ambulance', 1, 'doctor', 'EMT', 40, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(24, 'ambulance', 2, 'chief_doctor', 'Sr. EMT', 60, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(25, 'ambulance', 3, 'boss', 'EMT Supervisor', 80, '{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}', '{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
	(31, 'taxi', 0, 'recrue', 'Recruit', 12, '{"hair_2":0,"hair_color_2":0,"torso_1":32,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":31,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":0,"age_2":0,"glasses_2":0,"ears_2":0,"arms":27,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(32, 'taxi', 1, 'novice', 'Cabby', 24, '{"hair_2":0,"hair_color_2":0,"torso_1":32,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":31,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":0,"age_2":0,"glasses_2":0,"ears_2":0,"arms":27,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(33, 'taxi', 2, 'experimente', 'Experienced', 36, '{"hair_2":0,"hair_color_2":0,"torso_1":26,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":57,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":4,"age_2":0,"glasses_2":0,"ears_2":0,"arms":11,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":0,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(34, 'taxi', 3, 'uber', 'Uber Cabby', 48, '{"hair_2":0,"hair_color_2":0,"torso_1":26,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":57,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":4,"age_2":0,"glasses_2":0,"ears_2":0,"arms":11,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":0,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(35, 'taxi', 4, 'boss', 'Lead Cabby', 0, '{"hair_2":0,"hair_color_2":0,"torso_1":29,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":31,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":4,"age_2":0,"glasses_2":0,"ears_2":0,"arms":1,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":0,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":0,"bproof_1":0,"mask_1":0,"decals_1":0,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":4,"eyebrows_1":0,"face":0,"shoes_1":10,"pants_1":24}', '{"hair_2":0,"hair_color_2":0,"torso_1":57,"bags_1":0,"helmet_2":0,"chain_2":0,"eyebrows_3":0,"makeup_3":0,"makeup_2":0,"tshirt_1":38,"makeup_1":0,"bags_2":0,"makeup_4":0,"eyebrows_4":0,"chain_1":0,"lipstick_4":0,"bproof_2":0,"hair_color_1":0,"decals_2":0,"pants_2":1,"age_2":0,"glasses_2":0,"ears_2":0,"arms":21,"lipstick_1":0,"ears_1":-1,"mask_2":0,"sex":1,"lipstick_3":0,"helmet_1":-1,"shoes_2":0,"beard_2":0,"beard_1":0,"lipstick_2":0,"beard_4":0,"glasses_1":5,"bproof_1":0,"mask_1":0,"decals_1":1,"hair_1":0,"eyebrows_2":0,"beard_3":0,"age_1":0,"tshirt_2":0,"skin":0,"torso_2":0,"eyebrows_1":0,"face":0,"shoes_1":49,"pants_1":11}'),
	(40, 'garbage', 0, 'employee', 'Employee', 20, '{}', '{}'),
	(41, 'cardealer', 0, 'recruit', 'Recruit', 10, '{}', '{}'),
	(42, 'cardealer', 1, 'novice', 'Novice', 25, '{}', '{}'),
	(43, 'cardealer', 2, 'experienced', 'Experienced', 40, '{}', '{}'),
	(44, 'cardealer', 3, 'boss', 'Boss', 0, '{}', '{}'),
	(45, 'mechanic', 0, 'recrue', 'Recruit', 200, '{}', '{}'),
	(46, 'mechanic', 1, 'novice', 'Novice', 300, '{}', '{}'),
	(47, 'mechanic', 2, 'experienced', 'Experienced', 400, '{}', '{}'),
	(48, 'mechanic', 3, 'chief', 'Chief', 600, '{}', '{}'),
	(49, 'mechanic', 4, 'boss', 'Boss', 1000, '{}', '{}'),
	(50, 'shop', 0, 'apprentice', 'Apprentice', 200, '{}', '{}'),
	(51, 'shop', 1, 'clerk', 'Clerk', 300, '{}', '{}'),
	(52, 'shop', 2, 'boss', 'Owner', 400, '{}', '{}');
/*!40000 ALTER TABLE `job_grades` ENABLE KEYS */;

-- Dumping structure for table junsdb.licenses
CREATE TABLE IF NOT EXISTS `licenses` (
  `type` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.licenses: ~1 rows (approximately)
/*!40000 ALTER TABLE `licenses` DISABLE KEYS */;
INSERT INTO `licenses` (`type`, `label`) VALUES
	('weed_processing', 'Weed Processing License');
/*!40000 ALTER TABLE `licenses` ENABLE KEYS */;

-- Dumping structure for table junsdb.mail_accounts
CREATE TABLE IF NOT EXISTS `mail_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL,
  `createdBy` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.mail_accounts: ~1 rows (approximately)
/*!40000 ALTER TABLE `mail_accounts` DISABLE KEYS */;
INSERT INTO `mail_accounts` (`id`, `mail`, `password`, `createdBy`) VALUES
	(1, 'test@myMail.de', 'test1234', 'LICENSE');
/*!40000 ALTER TABLE `mail_accounts` ENABLE KEYS */;

-- Dumping structure for table junsdb.mail_messages
CREATE TABLE IF NOT EXISTS `mail_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` varchar(60) NOT NULL,
  `sender` varchar(120) NOT NULL,
  `receiver` varchar(120) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `content` text DEFAULT NULL,
  `isRead` tinyint(4) DEFAULT 0,
  `sendBy` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.mail_messages: ~0 rows (approximately)
/*!40000 ALTER TABLE `mail_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_messages` ENABLE KEYS */;

-- Dumping structure for table junsdb.owned_vehicles
CREATE TABLE IF NOT EXISTS `owned_vehicles` (
  `owner` varchar(22) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'car',
  `job` varchar(20) DEFAULT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  `date` varchar(255) DEFAULT NULL,
  `paidprice` int(11) NOT NULL DEFAULT 0,
  `finance` int(32) NOT NULL DEFAULT 0,
  `repaytime` int(32) NOT NULL DEFAULT 0,
  `model` varchar(60) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `storedhouse` int(11) NOT NULL,
  `insurance` int(11) NOT NULL DEFAULT 0,
  `gotKey` int(11) NOT NULL DEFAULT 0,
  `alarm` int(11) NOT NULL DEFAULT 0,
  `health` longtext NOT NULL DEFAULT '[]',
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.owned_vehicles: ~6 rows (approximately)
/*!40000 ALTER TABLE `owned_vehicles` DISABLE KEYS */;
INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`, `date`, `paidprice`, `finance`, `repaytime`, `model`, `state`, `storedhouse`, `insurance`, `gotKey`, `alarm`, `health`) VALUES
	('steam:11000010c863ac5', '48PGA426', '{"modBrakes":-1,"modTrimA":-1,"modDashboard":-1,"modTransmission":-1,"modAerials":-1,"wheels":7,"modFrontBumper":-1,"modLivery":-1,"tyreSmokeColor":[255,255,255],"modArmor":-1,"modHorns":-1,"modRightFender":-1,"modSuspension":-1,"modSmokeEnabled":false,"modDoorSpeaker":-1,"modOrnaments":-1,"modSideSkirt":-1,"modAPlate":-1,"pearlescentColor":111,"extras":[],"modFrame":-1,"modWindows":-1,"plate":"48PGA426","modTank":-1,"modTrunk":-1,"modPlateHolder":-1,"wheelColor":156,"health":1000,"modRearBumper":-1,"modFender":-1,"modRoof":-1,"dirtLevel":3.0,"modEngineBlock":-1,"modExhaust":-1,"modStruts":-1,"modTurbo":false,"model":-1403128555,"color2":41,"plateIndex":0,"modFrontWheels":-1,"neonEnabled":[false,false,false,false],"modArchCover":-1,"modSpoilers":-1,"modHood":-1,"modHydrolic":-1,"modTrimB":-1,"modEngine":-1,"modSeats":-1,"windowTint":-1,"modShifterLeavers":-1,"color1":4,"modBackWheels":-1,"neonColor":[255,0,255],"modXenon":false,"modSteeringWheel":-1,"modDial":-1,"modSpeakers":-1,"modAirFilter":-1,"modGrille":-1,"modVanityPlate":-1}', 'car', NULL, 0, '2021-02-18', 1650000, 0, 0, 'zentorno', 0, 0, 1, 1, 2, '[{"part":"brakes","value":100},{"part":"radiator","value":100},{"part":"clutch","value":100},{"part":"transmission","value":100},{"part":"electronics","value":100},{"part":"driveshaft","value":100},{"part":"fuelinjector","value":100},{"part":"engine","value":1000}]'),
	('steam:11000010c863ac5', '84DLJ029', '{"modAPlate":-1,"modStruts":-1,"modExhaust":3,"modTrimB":-1,"modHood":2,"plateIndex":0,"modXenon":1,"model":-998177792,"modSideSkirt":-1,"modBackWheels":-1,"modSuspension":4,"modTrimA":-1,"dirtLevel":6.11159420013427,"tyreSmokeColor":[255,255,255],"modTank":-1,"modEngine":3,"modLivery":-1,"modFrame":-1,"modGrille":-1,"modSpoilers":4,"modArmor":4,"modFender":-1,"modRoof":-1,"wheelColor":1,"neonEnabled":[1,1,1,1],"health":1000,"modSpeakers":-1,"modFrontBumper":4,"modOrnaments":-1,"extras":[],"modRightFender":-1,"windowTint":2,"modAirFilter":-1,"modDial":-1,"modWindows":-1,"plate":"84DLJ029","modVanityPlate":-1,"modRearBumper":4,"modSteeringWheel":-1,"modDoorSpeaker":-1,"modTransmission":2,"modSeats":-1,"color2":53,"neonColor":[0,255,0],"modTrunk":-1,"modEngineBlock":-1,"modShifterLeavers":-1,"wheels":7,"modTurbo":1,"color1":0,"modSmokeEnabled":1,"modHydrolic":-1,"modDashboard":-1,"modHorns":-1,"modPlateHolder":-1,"modFrontWheels":-1,"modAerials":-1,"pearlescentColor":53,"modBrakes":2,"modArchCover":-1}', 'car', NULL, 0, NULL, 0, 0, 0, '', 0, 0, 0, 0, 0, '[]'),
	('steam:11000010c863ac5', '99TIT570', '{"health":1000,"modFender":-1,"pearlescentColor":18,"modTransmission":-1,"modPlateHolder":-1,"plateIndex":0,"windowTint":-1,"modSteeringWheel":-1,"modArchCover":-1,"modDial":-1,"modRearBumper":-1,"modTank":-1,"modGrille":-1,"modSeats":-1,"modHorns":-1,"modBrakes":-1,"modAPlate":-1,"modSpoilers":-1,"modXenon":false,"modAerials":-1,"modTrimA":-1,"modTrimB":-1,"color1":0,"color2":6,"modSpeakers":-1,"plate":"99TIT570","modSuspension":-1,"modDashboard":-1,"modSideSkirt":-1,"modExhaust":-1,"modHood":-1,"modEngineBlock":-1,"modHydrolic":-1,"modFrontWheels":-1,"modDoorSpeaker":-1,"dirtLevel":3.0,"modEngine":-1,"modLivery":-1,"modStruts":-1,"modOrnaments":-1,"modBackWheels":-1,"wheels":0,"extras":[],"modVanityPlate":-1,"modWindows":-1,"modAirFilter":-1,"neonColor":[255,0,255],"modTurbo":false,"modFrame":-1,"wheelColor":156,"neonEnabled":[false,false,false,false],"modTrunk":-1,"modRoof":-1,"model":989294410,"modShifterLeavers":-1,"modArmor":-1,"tyreSmokeColor":[255,255,255],"modSmokeEnabled":false,"modRightFender":-1,"modFrontBumper":-1}', 'car', NULL, 0, '2021-02-19', 4213440, 0, 0, 'voltic2', 0, 0, 0, 0, 0, '[{"value":80,"part":"brakes"},{"value":95,"part":"radiator"},{"value":100,"part":"clutch"},{"value":100,"part":"transmission"},{"value":100,"part":"electronics"},{"value":84,"part":"driveshaft"},{"value":100,"part":"fuelinjector"},{"value":1000,"part":"engine"}]'),
	('steam:11000010c863ac5', 'DFW 671', '{"plate":"DFW 671","model":2123327359}', 'car', NULL, 0, NULL, 0, 0, 0, '', 0, 0, 0, 0, 0, '[]'),
	('steam:11000010c863ac5', 'IOC 502', '{"plate":"IOC 502","model":2123327359}', 'car', NULL, 0, NULL, 0, 0, 0, '', 0, 0, 0, 0, 0, '[]'),
	('steam:11000010c863ac5', 'JQD 060', '{"plate":"JQD 060","model":2123327359}', 'car', NULL, 0, NULL, 0, 0, 0, '', 0, 0, 0, 0, 0, '[]');
/*!40000 ALTER TABLE `owned_vehicles` ENABLE KEYS */;

-- Dumping structure for table junsdb.pawnshops
CREATE TABLE IF NOT EXISTS `pawnshops` (
  `shopdata` longtext DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.pawnshops: ~3 rows (approximately)
/*!40000 ALTER TABLE `pawnshops` DISABLE KEYS */;
INSERT INTO `pawnshops` (`shopdata`, `id`) VALUES
	('{"sell":[{"price":100,"item":"weed","label":"Weed","max":50,"count":0},{"price":50,"item":"meth","label":"Meth","max":10,"count":0}],"buy":[{"price":200,"item":"weed","label":"Weed","count":0,"max":50,"startcount":0},{"price":100,"item":"meth","label":"Meth","count":0,"max":10,"startcount":0}],"text":"Press [~r~E~s~] to interact with the Drug Shop.","blip":false,"title":"Drug Shop","loc":{"y":-1571.95,"z":4.66,"x":-1172.01}}', 1),
	('{"sell":[{"price":500,"item":"lockpick","label":"Lockpick","max":50,"count":0},{"price":200,"item":"rolex","label":"Rolex","max":10,"count":0},{"price":100,"item":"oxycutter","label":"Oxycutter","max":10,"count":0}],"buy":[{"price":100,"item":"lockpick","label":"Lockpick","count":0,"max":50,"startcount":5},{"price":100,"item":"rolex","label":"Rolex","count":0,"max":10,"startcount":0},{"price":100,"item":"oxycutter","label":"Oxycutter","count":0,"max":10,"startcount":0}],"text":"Press [~r~E~s~] to interact with the Pawn Shop.","blip":true,"title":"Pawn Shop","loc":{"y":315.11,"z":103.13,"x":412.23}}', 2),
	('{"sell":[{"price":1000,"item":"weapon_minigun","weapon":true,"label":"Minigun","max":1,"count":0},{"price":5000,"item":"weapon_assaultshotgun","weapon":true,"label":"Assault Shotgun","max":5,"count":0},{"price":5000,"item":"weapon_pumpshotgun","weapon":true,"label":"Pump Shotgun","max":5,"count":0}],"buy":[{"price":50000,"item":"weapon_minigun","weapon":true,"label":"Minigun","count":0,"max":1,"startcount":0},{"price":15000,"item":"weapon_assaultshotgun","weapon":true,"label":"Assault Shotgun","count":0,"max":5,"startcount":0},{"price":15000,"item":"weapon_pumpshotgun","weapon":true,"label":"Pump Shotgun","count":0,"max":5,"startcount":0}],"text":"Press [~r~E~s~] to interact with the Black Market.","blip":false,"title":"Black Market","loc":{"y":-2171.67,"z":30.27,"x":830.43}}', 3);
/*!40000 ALTER TABLE `pawnshops` ENABLE KEYS */;

-- Dumping structure for table junsdb.phone_app_chat
CREATE TABLE IF NOT EXISTS `phone_app_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.phone_app_chat: ~0 rows (approximately)
/*!40000 ALTER TABLE `phone_app_chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_app_chat` ENABLE KEYS */;

-- Dumping structure for table junsdb.phone_calls
CREATE TABLE IF NOT EXISTS `phone_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.phone_calls: ~0 rows (approximately)
/*!40000 ALTER TABLE `phone_calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_calls` ENABLE KEYS */;

-- Dumping structure for table junsdb.phone_messages
CREATE TABLE IF NOT EXISTS `phone_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.phone_messages: 0 rows
/*!40000 ALTER TABLE `phone_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_messages` ENABLE KEYS */;

-- Dumping structure for table junsdb.phone_users_contacts
CREATE TABLE IF NOT EXISTS `phone_users_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.phone_users_contacts: 0 rows
/*!40000 ALTER TABLE `phone_users_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `phone_users_contacts` ENABLE KEYS */;

-- Dumping structure for table junsdb.rented_vehicles
CREATE TABLE IF NOT EXISTS `rented_vehicles` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.rented_vehicles: ~0 rows (approximately)
/*!40000 ALTER TABLE `rented_vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `rented_vehicles` ENABLE KEYS */;

-- Dumping structure for table junsdb.shops
CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.shops: ~6 rows (approximately)
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;
INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
	(1, 'TwentyFourSeven', 'bread', 30),
	(2, 'TwentyFourSeven', 'water', 15),
	(3, 'RobsLiquor', 'bread', 30),
	(4, 'RobsLiquor', 'water', 15),
	(5, 'LTDgasoline', 'bread', 30),
	(6, 'LTDgasoline', 'water', 15);
/*!40000 ALTER TABLE `shops` ENABLE KEYS */;

-- Dumping structure for table junsdb.society_moneywash
CREATE TABLE IF NOT EXISTS `society_moneywash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.society_moneywash: ~0 rows (approximately)
/*!40000 ALTER TABLE `society_moneywash` DISABLE KEYS */;
/*!40000 ALTER TABLE `society_moneywash` ENABLE KEYS */;

-- Dumping structure for table junsdb.t1ger_deliveryjob
CREATE TABLE IF NOT EXISTS `t1ger_deliveryjob` (
  `identifier` varchar(100) CHARACTER SET latin1 NOT NULL,
  `companyID` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'Delivery Company',
  `level` int(11) NOT NULL DEFAULT 0,
  `certificate` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`companyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.t1ger_deliveryjob: ~0 rows (approximately)
/*!40000 ALTER TABLE `t1ger_deliveryjob` DISABLE KEYS */;
/*!40000 ALTER TABLE `t1ger_deliveryjob` ENABLE KEYS */;

-- Dumping structure for table junsdb.t1ger_mechanic
CREATE TABLE IF NOT EXISTS `t1ger_mechanic` (
  `identifier` varchar(100) CHARACTER SET latin1 NOT NULL,
  `shopID` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'Mechanic Shop',
  `money` int(11) NOT NULL DEFAULT 0,
  `employees` longtext NOT NULL DEFAULT '[]',
  `storage` longtext NOT NULL DEFAULT '[]',
  PRIMARY KEY (`shopID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.t1ger_mechanic: ~0 rows (approximately)
/*!40000 ALTER TABLE `t1ger_mechanic` DISABLE KEYS */;
/*!40000 ALTER TABLE `t1ger_mechanic` ENABLE KEYS */;

-- Dumping structure for table junsdb.t1ger_orders
CREATE TABLE IF NOT EXISTS `t1ger_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopID` int(11) DEFAULT NULL,
  `data` longtext NOT NULL DEFAULT '[]',
  `taken` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.t1ger_orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `t1ger_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `t1ger_orders` ENABLE KEYS */;

-- Dumping structure for table junsdb.t1ger_shops
CREATE TABLE IF NOT EXISTS `t1ger_shops` (
  `identifier` varchar(100) CHARACTER SET latin1 NOT NULL,
  `shopID` int(11) NOT NULL,
  `money` int(11) NOT NULL DEFAULT 0,
  `employees` longtext NOT NULL DEFAULT '[]',
  `stock` longtext NOT NULL DEFAULT '[]',
  `shelves` longtext NOT NULL DEFAULT '[]',
  PRIMARY KEY (`shopID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.t1ger_shops: ~0 rows (approximately)
/*!40000 ALTER TABLE `t1ger_shops` DISABLE KEYS */;
/*!40000 ALTER TABLE `t1ger_shops` ENABLE KEYS */;

-- Dumping structure for table junsdb.taxbills
CREATE TABLE IF NOT EXISTS `taxbills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3443 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=COMPACT;

-- Dumping data for table junsdb.taxbills: ~0 rows (approximately)
/*!40000 ALTER TABLE `taxbills` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxbills` ENABLE KEYS */;

-- Dumping structure for table junsdb.twitter_accounts
CREATE TABLE IF NOT EXISTS `twitter_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table junsdb.twitter_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `twitter_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_accounts` ENABLE KEYS */;

-- Dumping structure for table junsdb.twitter_likes
CREATE TABLE IF NOT EXISTS `twitter_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  KEY `FK_twitter_likes_twitter_tweets` (`tweetId`),
  CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table junsdb.twitter_likes: ~0 rows (approximately)
/*!40000 ALTER TABLE `twitter_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_likes` ENABLE KEYS */;

-- Dumping structure for table junsdb.twitter_tweets
CREATE TABLE IF NOT EXISTS `twitter_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_twitter_tweets_twitter_accounts` (`authorId`),
  CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table junsdb.twitter_tweets: ~0 rows (approximately)
/*!40000 ALTER TABLE `twitter_tweets` DISABLE KEYS */;
/*!40000 ALTER TABLE `twitter_tweets` ENABLE KEYS */;

-- Dumping structure for table junsdb.users
CREATE TABLE IF NOT EXISTS `users` (
  `identifier` varchar(80) NOT NULL,
  `accounts` longtext DEFAULT NULL,
  `group` varchar(50) DEFAULT 'user',
  `inventory` longtext DEFAULT NULL,
  `job` varchar(20) DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext DEFAULT NULL,
  `position` varchar(255) DEFAULT '{"x":-269.4,"y":-955.3,"z":31.2,"heading":205.8}',
  `firstname` varchar(16) DEFAULT NULL,
  `lastname` varchar(16) DEFAULT NULL,
  `dateofbirth` varchar(10) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `skin` longtext DEFAULT NULL,
  `status` longtext DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `phone_number` varchar(10) DEFAULT NULL,
  `license` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT '',
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `pedModeAllowed` tinyint(4) DEFAULT NULL,
  `pedModel` varchar(255) DEFAULT NULL,
  `tattoos` longtext DEFAULT NULL,
  `skellydata` longtext NOT NULL,
  PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`identifier`, `accounts`, `group`, `inventory`, `job`, `job_grade`, `loadout`, `position`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `skin`, `status`, `is_dead`, `phone_number`, `license`, `money`, `name`, `bank`, `permission_level`, `pedModeAllowed`, `pedModel`, `tattoos`, `skellydata`) VALUES
	('steam:11000010c863ac5', NULL, 'superadmin', NULL, 'cardealer', 3, '[]', '{"x":-1151.5,"y":-1426.4,"z":10.4}', 'Test', 'Testing', '15/03/1997', 'm', 180, '{"headOverlay":[{"name":"Blemishes","overlayValue":255,"firstColour":0,"colourType":0,"secondColour":0,"overlayOpacity":1.0},{"name":"FacialHair","overlayValue":255,"firstColour":0,"colourType":1,"secondColour":0,"overlayOpacity":0.0},{"name":"Eyebrows","overlayValue":255,"firstColour":0,"colourType":1,"secondColour":0,"overlayOpacity":1.0},{"name":"Ageing","overlayValue":255,"firstColour":0,"colourType":0,"secondColour":0,"overlayOpacity":1.0},{"name":"Makeup","overlayValue":255,"firstColour":0,"colourType":2,"secondColour":0,"overlayOpacity":1.0},{"name":"Blush","overlayValue":255,"firstColour":0,"colourType":2,"secondColour":0,"overlayOpacity":1.0},{"name":"Complexion","overlayValue":255,"firstColour":0,"colourType":0,"secondColour":0,"overlayOpacity":1.0},{"name":"SunDamage","overlayValue":255,"firstColour":0,"colourType":0,"secondColour":0,"overlayOpacity":1.0},{"name":"Lipstick","overlayValue":255,"firstColour":0,"colourType":2,"secondColour":0,"overlayOpacity":1.0},{"name":"MolesFreckles","overlayValue":255,"firstColour":0,"colourType":0,"secondColour":0,"overlayOpacity":1.0},{"name":"ChestHair","overlayValue":255,"firstColour":0,"colourType":1,"secondColour":0,"overlayOpacity":1.0},{"name":"BodyBlemishes","overlayValue":255,"firstColour":0,"colourType":0,"secondColour":0,"overlayOpacity":1.0},{"name":"AddBodyBlemishes","overlayValue":255,"firstColour":0,"colourType":0,"secondColour":0,"overlayOpacity":1.0}],"hairColor":[1,1],"headBlend":{"skinThird":0,"thirdMix":0.0,"shapeSecond":0,"shapeMix":0.0,"hasParent":false,"skinSecond":0,"skinFirst":15,"shapeThird":0,"shapeFirst":0,"skinMix":1.0},"model":1885233650,"proptextures":[["hats",-1],["glasses",0],["earrings",-1],["mouth",-1],["lhand",-1],["rhand",-1],["watches",-1],["braclets",-1]],"headStructure":[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],"props":{"1":["glasses",18],"2":["earrings",-1],"3":["mouth",-1],"4":["lhand",-1],"5":["rhand",-1],"6":["watches",-1],"7":["braclets",-1],"0":["hats",-1]},"drawables":{"1":["masks",0],"2":["hair",46],"3":["torsos",15],"4":["legs",62],"5":["bags",0],"6":["shoes",1],"7":["neck",0],"8":["undershirts",15],"9":["vest",0],"10":["decals",0],"11":["jackets",-1],"0":["face",0]},"drawtextures":[["face",0],["masks",0],["hair",0],["torsos",0],["legs",0],["bags",0],["shoes",2],["neck",0],["undershirts",0],["vest",0],["decals",0],["jackets",0]]}', '[]', 0, '916-3541', 'license:4ca93f5c00be168bc1e4bf2f2bde943f208c2943', 84096788, '21 Flower (Juns)', 99926299, 0, NULL, NULL, '[{"Count":1,"nameHash":"MP_Bea_M_Head_002","collection":"mpbeach_overlays"},{"Count":1,"nameHash":"MP_MP_Biker_Tat_009_M","collection":"mpbiker_overlays"},{"Count":1,"nameHash":"MP_MP_Biker_Tat_038_M","collection":"mpbiker_overlays"},{"Count":1,"nameHash":"MP_Buis_M_Neck_000","collection":"mpbusiness_overlays"},{"Count":1,"nameHash":"MP_Smuggler_Tattoo_011_M","collection":"mpsmuggler_overlays"},{"Count":1,"nameHash":"MP_Gunrunning_Tattoo_023_M","collection":"mpgunrunning_overlays"},{"Count":1,"nameHash":"MP_MP_Biker_Tat_056_M","collection":"mpbiker_overlays"},{"Count":1,"nameHash":"MP_MP_Stunt_tat_007_M","collection":"mpstunt_overlays"},{"Count":1,"nameHash":"MP_Christmas2017_Tattoo_029_M","collection":"mpchristmas2017_overlays"},{"Count":1,"nameHash":"MP_Xmas2_M_Tat_020","collection":"mpchristmas2_overlays"},{"Count":1,"nameHash":"MP_Gunrunning_Tattoo_016_M","collection":"mpgunrunning_overlays"},{"Count":1,"nameHash":"MP_LR_Tat_030_M","collection":"mplowrider2_overlays"},{"Count":1,"nameHash":"FM_Tat_M_040","collection":"multiplayer_overlays"},{"Count":1,"nameHash":"MP_Xmas2_M_Tat_026","collection":"mpchristmas2_overlays"},{"Count":1,"nameHash":"MP_LR_Tat_028_M","collection":"mplowrider2_overlays"},{"Count":1,"nameHash":"FM_Tat_M_000","collection":"multiplayer_overlays"},{"Count":1,"nameHash":"FM_Tat_M_014","collection":"multiplayer_overlays"},{"Count":1,"nameHash":"MP_MP_Biker_Tat_030_M","collection":"mpbiker_overlays"},{"Count":1,"nameHash":"FM_Hip_M_Tat_011","collection":"mphipster_overlays"},{"Count":1,"nameHash":"MP_LR_Tat_000_M","collection":"mplowrider2_overlays"},{"Count":1,"nameHash":"MP_LR_Tat_013_M","collection":"mplowrider_overlays"},{"Count":1,"nameHash":"FM_Tat_M_004","collection":"multiplayer_overlays"},{"Count":1,"nameHash":"MP_LR_Tat_012_M","collection":"mplowrider2_overlays"},{"Count":1,"nameHash":"MP_MP_Biker_Tat_041_M","collection":"mpbiker_overlays"}]', '{"LeftLeg":0,"Head":0,"RightLeg":0,"Body":0,"RightArm":0,"LeftArm":0}');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table junsdb.user_accounts
CREATE TABLE IF NOT EXISTS `user_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(80) NOT NULL,
  `name` varchar(50) NOT NULL,
  `money` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.user_accounts: ~2 rows (approximately)
/*!40000 ALTER TABLE `user_accounts` DISABLE KEYS */;
INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
	(3, 'Char1:11000010c863ac5', 'black_money', 0),
	(4, 'steam:11000010c863ac5', 'black_money', 0);
/*!40000 ALTER TABLE `user_accounts` ENABLE KEYS */;

-- Dumping structure for table junsdb.user_clothes
CREATE TABLE IF NOT EXISTS `user_clothes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `clothesData` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Dumping data for table junsdb.user_clothes: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_clothes` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_clothes` ENABLE KEYS */;

-- Dumping structure for table junsdb.user_inventory
CREATE TABLE IF NOT EXISTS `user_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(80) NOT NULL,
  `item` varchar(50) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.user_inventory: ~272 rows (approximately)
/*!40000 ALTER TABLE `user_inventory` DISABLE KEYS */;
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
	(1, 'Char1:11000010c863ac5', 'WEAPON_SWITCHBLADE', 0),
	(2, 'Char1:11000010c863ac5', 'WEAPON_HEAVYSHOTGUN', 0),
	(3, 'Char1:11000010c863ac5', 'WEAPON_BOTTLE', 0),
	(4, 'Char1:11000010c863ac5', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(5, 'Char1:11000010c863ac5', 'WEAPON_HANDCUFFS', 0),
	(6, 'Char1:11000010c863ac5', 'Phone', 0),
	(7, 'Char1:11000010c863ac5', 'WEAPON_KNIFE', 0),
	(8, 'Char1:11000010c863ac5', 'cyber_ammo_shotgun_large', 0),
	(9, 'Char1:11000010c863ac5', 'WEAPON_MOLOTOV', 0),
	(10, 'Char1:11000010c863ac5', 'WEAPON_BATTLEAXE', 0),
	(11, 'Char1:11000010c863ac5', 'cyber_ammo_shotgun', 0),
	(12, 'Char1:11000010c863ac5', 'bulletproof', 0),
	(13, 'Char1:11000010c863ac5', 'WEAPON_VINTAGEPISTOL', 0),
	(14, 'Char1:11000010c863ac5', 'WEAPON_PETROLCAN', 0),
	(15, 'Char1:11000010c863ac5', 'WEAPON_MG', 0),
	(16, 'Char1:11000010c863ac5', 'WEAPON_COMBATPISTOL', 0),
	(17, 'Char1:11000010c863ac5', 'cupcake', 0),
	(18, 'Char1:11000010c863ac5', 'WEAPON_PISTOL', 0),
	(19, 'Char1:11000010c863ac5', 'chocolate', 0),
	(20, 'Char1:11000010c863ac5', 'WEAPON_COMPACTLAUNCHER', 0),
	(21, 'Char1:11000010c863ac5', 'suppressor', 0),
	(22, 'Char1:11000010c863ac5', 'WEAPON_BAT', 0),
	(23, 'Char1:11000010c863ac5', 'WEAPON_PIPEBOMB', 0),
	(24, 'Char1:11000010c863ac5', 'milk', 0),
	(25, 'Char1:11000010c863ac5', 'WEAPON_RPG', 0),
	(26, 'Char1:11000010c863ac5', 'grip', 0),
	(27, 'Char1:11000010c863ac5', 'water', 0),
	(28, 'Char1:11000010c863ac5', 'WEAPON_SNSPISTOL', 0),
	(29, 'Char1:11000010c863ac5', 'WEAPON_APPISTOL', 0),
	(30, 'Char1:11000010c863ac5', 'WEAPON_MARKSMANPISTOL', 0),
	(31, 'Char1:11000010c863ac5', 'cigarett', 0),
	(32, 'Char1:11000010c863ac5', 'WEAPON_HOMINGLAUNCHER', 0),
	(33, 'Char1:11000010c863ac5', 'WEAPON_GRENADELAUNCHER', 0),
	(34, 'Char1:11000010c863ac5', 'WEAPON_GARBAGEBAG', 0),
	(35, 'Char1:11000010c863ac5', 'WEAPON_PUMPSHOTGUN', 0),
	(36, 'Char1:11000010c863ac5', 'WEAPON_WRENCH', 0),
	(37, 'Char1:11000010c863ac5', 'WEAPON_STUNGUN', 0),
	(38, 'Char1:11000010c863ac5', 'WEAPON_SMOKEGRENADE', 0),
	(39, 'Char1:11000010c863ac5', 'WEAPON_FLASHLIGHT', 0),
	(40, 'Char1:11000010c863ac5', 'cyber_ammo_pistol_large', 0),
	(41, 'Char1:11000010c863ac5', 'beer', 0),
	(42, 'Char1:11000010c863ac5', 'WEAPON_STINGER', 0),
	(43, 'Char1:11000010c863ac5', 'WEAPON_GRENADE', 0),
	(44, 'Char1:11000010c863ac5', 'WEAPON_BULLPUPSHOTGUN', 0),
	(45, 'Char1:11000010c863ac5', 'WEAPON_MICROSMG', 0),
	(46, 'Char1:11000010c863ac5', 'WEAPON_ASSAULTSMG', 0),
	(47, 'Char1:11000010c863ac5', 'WEAPON_SPECIALCARBINE', 0),
	(48, 'Char1:11000010c863ac5', 'WEAPON_FIREWORK', 0),
	(49, 'Char1:11000010c863ac5', 'WEAPON_SNOWBALL', 0),
	(50, 'Char1:11000010c863ac5', 'WEAPON_SNIPERRIFLE', 0),
	(51, 'Char1:11000010c863ac5', 'WEAPON_FLARE', 0),
	(52, 'Char1:11000010c863ac5', 'cyber_ammo_rifle_large', 0),
	(53, 'Char1:11000010c863ac5', 'WEAPON_HAMMER', 0),
	(54, 'Char1:11000010c863ac5', 'WEAPON_SMG', 0),
	(55, 'Char1:11000010c863ac5', 'WEAPON_MACHETE', 0),
	(56, 'Char1:11000010c863ac5', 'WEAPON_REVOLVER', 0),
	(57, 'Char1:11000010c863ac5', 'WEAPON_COMPACTRIFLE', 0),
	(58, 'Char1:11000010c863ac5', 'WEAPON_ADVANCEDRIFLE', 0),
	(59, 'Char1:11000010c863ac5', 'cyber_ammo_snp_large', 0),
	(60, 'Char1:11000010c863ac5', 'WEAPON_RAILGUN', 0),
	(61, 'Char1:11000010c863ac5', 'WEAPON_HATCHET', 0),
	(62, 'Char1:11000010c863ac5', 'WEAPON_PROXMINE', 0),
	(63, 'Char1:11000010c863ac5', 'cyber_ammo_snp', 0),
	(64, 'Char1:11000010c863ac5', 'WEAPON_ASSAULTRIFLE', 0),
	(65, 'Char1:11000010c863ac5', 'WEAPON_DBSHOTGUN', 0),
	(66, 'Char1:11000010c863ac5', 'WEAPON_PISTOL50', 0),
	(67, 'Char1:11000010c863ac5', 'coffee', 0),
	(68, 'Char1:11000010c863ac5', 'WEAPON_AUTOSHOTGUN', 0),
	(69, 'Char1:11000010c863ac5', 'WEAPON_NIGHTSTICK', 0),
	(70, 'Char1:11000010c863ac5', 'chips', 0),
	(71, 'Char1:11000010c863ac5', 'WEAPON_MUSKET', 0),
	(72, 'Char1:11000010c863ac5', 'goldtint', 0),
	(73, 'Char1:11000010c863ac5', 'WEAPON_MINISMG', 0),
	(74, 'Char1:11000010c863ac5', 'cyber_ammo_rifle', 0),
	(75, 'Char1:11000010c863ac5', 'WEAPON_DIGISCANNER', 0),
	(76, 'Char1:11000010c863ac5', 'cocacola', 0),
	(77, 'Char1:11000010c863ac5', 'WEAPON_FIREEXTINGUISHER', 0),
	(78, 'Char1:11000010c863ac5', 'WEAPON_MINIGUN', 0),
	(79, 'Char1:11000010c863ac5', 'WEAPON_DAGGER', 0),
	(80, 'Char1:11000010c863ac5', 'WEAPON_MARKSMANRIFLE', 0),
	(81, 'Char1:11000010c863ac5', 'WEAPON_MACHINEPISTOL', 0),
	(82, 'Char1:11000010c863ac5', 'WEAPON_KNUCKLE', 0),
	(83, 'Char1:11000010c863ac5', 'WEAPON_HEAVYSNIPER', 0),
	(84, 'Char1:11000010c863ac5', 'WEAPON_HEAVYPISTOL', 0),
	(85, 'Char1:11000010c863ac5', 'cyber_ammo_smg', 0),
	(86, 'Char1:11000010c863ac5', 'bread', 0),
	(87, 'Char1:11000010c863ac5', 'WEAPON_GUSENBERG', 0),
	(88, 'Char1:11000010c863ac5', 'WEAPON_BZGAS', 0),
	(89, 'Char1:11000010c863ac5', 'WEAPON_STICKYBOMB', 0),
	(90, 'Char1:11000010c863ac5', 'WEAPON_GOLFCLUB', 0),
	(91, 'Char1:11000010c863ac5', 'WEAPON_DOUBLEACTION', 0),
	(92, 'Char1:11000010c863ac5', 'cyber_ammo_pistol', 0),
	(93, 'Char1:11000010c863ac5', 'WEAPON_ASSAULTSHOTGUN', 0),
	(94, 'Char1:11000010c863ac5', 'WEAPON_BALL', 0),
	(95, 'Char1:11000010c863ac5', 'WEAPON_COMBATPDW', 0),
	(96, 'Char1:11000010c863ac5', 'WEAPON_FLAREGUN', 0),
	(97, 'Char1:11000010c863ac5', 'WEAPON_COMBATMG', 0),
	(98, 'Char1:11000010c863ac5', 'WEAPON_POOLCUE', 0),
	(99, 'Char1:11000010c863ac5', 'cash', 0),
	(100, 'Char1:11000010c863ac5', 'WEAPON_CROWBAR', 0),
	(101, 'Char1:11000010c863ac5', 'WEAPON_CARBINERIFLE', 0),
	(102, 'Char1:11000010c863ac5', 'lighter', 0),
	(103, 'Char1:11000010c863ac5', 'sight', 0),
	(104, 'Char1:11000010c863ac5', 'cyber_ammo_smg_large', 0),
	(105, 'Char1:11000010c863ac5', 'WEAPON_BULLPUPRIFLE', 0),
	(106, 'Char1:11000010c863ac5', 'light', 0),
	(107, 'Char1:11000010c863ac5', 'extended', 0),
	(108, 'steam:11000010c863ac5', 'WEAPON_STINGER', 0),
	(109, 'steam:11000010c863ac5', 'WEAPON_PUMPSHOTGUN', 0),
	(110, 'steam:11000010c863ac5', 'Phone', 0),
	(111, 'steam:11000010c863ac5', 'WEAPON_POOLCUE', 0),
	(112, 'steam:11000010c863ac5', 'WEAPON_ADVANCEDRIFLE', 0),
	(113, 'steam:11000010c863ac5', 'WEAPON_BULLPUPRIFLE', 0),
	(114, 'steam:11000010c863ac5', 'WEAPON_COMBATPISTOL', 0),
	(115, 'steam:11000010c863ac5', 'cyber_ammo_smg_large', 0),
	(116, 'steam:11000010c863ac5', 'WEAPON_PIPEBOMB', 0),
	(117, 'steam:11000010c863ac5', 'WEAPON_BALL', 0),
	(118, 'steam:11000010c863ac5', 'WEAPON_MINIGUN', 0),
	(119, 'steam:11000010c863ac5', 'sight', 1),
	(120, 'steam:11000010c863ac5', 'WEAPON_CROWBAR', 0),
	(121, 'steam:11000010c863ac5', 'WEAPON_ASSAULTSMG', 0),
	(122, 'steam:11000010c863ac5', 'chocolate', 0),
	(123, 'steam:11000010c863ac5', 'WEAPON_FIREEXTINGUISHER', 0),
	(124, 'steam:11000010c863ac5', 'light', 0),
	(125, 'steam:11000010c863ac5', 'WEAPON_BAT', 0),
	(126, 'steam:11000010c863ac5', 'WEAPON_APPISTOL', 0),
	(127, 'steam:11000010c863ac5', 'WEAPON_MOLOTOV', 0),
	(128, 'steam:11000010c863ac5', 'suppressor', 0),
	(129, 'steam:11000010c863ac5', 'WEAPON_SMG', 0),
	(130, 'steam:11000010c863ac5', 'WEAPON_FLAREGUN', 0),
	(131, 'steam:11000010c863ac5', 'WEAPON_HATCHET', 0),
	(132, 'steam:11000010c863ac5', 'cyber_ammo_snp', 0),
	(133, 'steam:11000010c863ac5', 'WEAPON_WRENCH', 0),
	(134, 'steam:11000010c863ac5', 'bread', 0),
	(135, 'steam:11000010c863ac5', 'WEAPON_BOTTLE', 0),
	(136, 'steam:11000010c863ac5', 'WEAPON_VINTAGEPISTOL', 0),
	(137, 'steam:11000010c863ac5', 'goldtint', 0),
	(138, 'steam:11000010c863ac5', 'water', 0),
	(139, 'steam:11000010c863ac5', 'milk', 0),
	(140, 'steam:11000010c863ac5', 'WEAPON_HEAVYPISTOL', 0),
	(141, 'steam:11000010c863ac5', 'WEAPON_MINISMG', 0),
	(142, 'steam:11000010c863ac5', 'WEAPON_STUNGUN', 0),
	(143, 'steam:11000010c863ac5', 'WEAPON_STICKYBOMB', 0),
	(144, 'steam:11000010c863ac5', 'cigarett', 0),
	(145, 'steam:11000010c863ac5', 'WEAPON_SPECIALCARBINE', 0),
	(146, 'steam:11000010c863ac5', 'WEAPON_PISTOL', 0),
	(147, 'steam:11000010c863ac5', 'WEAPON_SNOWBALL', 0),
	(148, 'steam:11000010c863ac5', 'cyber_ammo_shotgun_large', 0),
	(149, 'steam:11000010c863ac5', 'WEAPON_RPG', 0),
	(150, 'steam:11000010c863ac5', 'WEAPON_SNIPERRIFLE', 0),
	(151, 'steam:11000010c863ac5', 'WEAPON_CARBINERIFLE', 1),
	(152, 'steam:11000010c863ac5', 'WEAPON_SAWNOFFSHOTGUN', 0),
	(153, 'steam:11000010c863ac5', 'WEAPON_COMPACTRIFLE', 0),
	(154, 'steam:11000010c863ac5', 'WEAPON_GUSENBERG', 0),
	(155, 'steam:11000010c863ac5', 'WEAPON_DIGISCANNER', 0),
	(156, 'steam:11000010c863ac5', 'WEAPON_REVOLVER', 0),
	(157, 'steam:11000010c863ac5', 'WEAPON_HAMMER', 0),
	(158, 'steam:11000010c863ac5', 'WEAPON_HEAVYSNIPER', 0),
	(159, 'steam:11000010c863ac5', 'WEAPON_MUSKET', 0),
	(160, 'steam:11000010c863ac5', 'WEAPON_SNSPISTOL', 0),
	(161, 'steam:11000010c863ac5', 'WEAPON_MG', 0),
	(162, 'steam:11000010c863ac5', 'WEAPON_NIGHTSTICK', 0),
	(163, 'steam:11000010c863ac5', 'WEAPON_PISTOL50', 0),
	(164, 'steam:11000010c863ac5', 'WEAPON_MARKSMANPISTOL', 0),
	(165, 'steam:11000010c863ac5', 'WEAPON_MICROSMG', 0),
	(166, 'steam:11000010c863ac5', 'WEAPON_PETROLCAN', 0),
	(167, 'steam:11000010c863ac5', 'WEAPON_MARKSMANRIFLE', 0),
	(168, 'steam:11000010c863ac5', 'bulletproof', 0),
	(169, 'steam:11000010c863ac5', 'WEAPON_MACHINEPISTOL', 0),
	(170, 'steam:11000010c863ac5', 'WEAPON_HANDCUFFS', 0),
	(171, 'steam:11000010c863ac5', 'WEAPON_ASSAULTSHOTGUN', 0),
	(172, 'steam:11000010c863ac5', 'WEAPON_HEAVYSHOTGUN', 0),
	(173, 'steam:11000010c863ac5', 'cyber_ammo_rifle_large', 0),
	(174, 'steam:11000010c863ac5', 'WEAPON_MACHETE', 0),
	(175, 'steam:11000010c863ac5', 'cyber_ammo_pistol', 0),
	(176, 'steam:11000010c863ac5', 'WEAPON_KNUCKLE', 0),
	(177, 'steam:11000010c863ac5', 'WEAPON_BATTLEAXE', 0),
	(178, 'steam:11000010c863ac5', 'chips', 0),
	(179, 'steam:11000010c863ac5', 'WEAPON_ASSAULTRIFLE', 0),
	(180, 'steam:11000010c863ac5', 'extended', 0),
	(181, 'steam:11000010c863ac5', 'WEAPON_HOMINGLAUNCHER', 0),
	(182, 'steam:11000010c863ac5', 'WEAPON_PROXMINE', 0),
	(183, 'steam:11000010c863ac5', 'cash', 0),
	(184, 'steam:11000010c863ac5', 'WEAPON_RAILGUN', 0),
	(185, 'steam:11000010c863ac5', 'WEAPON_FIREWORK', 0),
	(186, 'steam:11000010c863ac5', 'WEAPON_GOLFCLUB', 0),
	(187, 'steam:11000010c863ac5', 'cyber_ammo_smg', 0),
	(188, 'steam:11000010c863ac5', 'cyber_ammo_snp_large', 0),
	(189, 'steam:11000010c863ac5', 'WEAPON_GRENADELAUNCHER', 0),
	(190, 'steam:11000010c863ac5', 'beer', 0),
	(191, 'steam:11000010c863ac5', 'coffee', 0),
	(192, 'steam:11000010c863ac5', 'lighter', 0),
	(193, 'steam:11000010c863ac5', 'cocacola', 0),
	(194, 'steam:11000010c863ac5', 'WEAPON_BULLPUPSHOTGUN', 0),
	(195, 'steam:11000010c863ac5', 'cyber_ammo_rifle', 0),
	(196, 'steam:11000010c863ac5', 'WEAPON_COMPACTLAUNCHER', 0),
	(197, 'steam:11000010c863ac5', 'WEAPON_GRENADE', 0),
	(198, 'steam:11000010c863ac5', 'WEAPON_AUTOSHOTGUN', 0),
	(199, 'steam:11000010c863ac5', 'WEAPON_SMOKEGRENADE', 0),
	(200, 'steam:11000010c863ac5', 'WEAPON_GARBAGEBAG', 0),
	(201, 'steam:11000010c863ac5', 'WEAPON_FLARE', 0),
	(202, 'steam:11000010c863ac5', 'WEAPON_BZGAS', 0),
	(203, 'steam:11000010c863ac5', 'WEAPON_FLASHLIGHT', 0),
	(204, 'steam:11000010c863ac5', 'WEAPON_COMBATMG', 0),
	(205, 'steam:11000010c863ac5', 'WEAPON_DBSHOTGUN', 0),
	(206, 'steam:11000010c863ac5', 'WEAPON_DOUBLEACTION', 0),
	(207, 'steam:11000010c863ac5', 'grip', 0),
	(208, 'steam:11000010c863ac5', 'WEAPON_KNIFE', 0),
	(209, 'steam:11000010c863ac5', 'WEAPON_DAGGER', 0),
	(210, 'steam:11000010c863ac5', 'cyber_ammo_shotgun', 0),
	(211, 'steam:11000010c863ac5', 'WEAPON_COMBATPDW', 0),
	(212, 'steam:11000010c863ac5', 'WEAPON_SWITCHBLADE', 0),
	(213, 'steam:11000010c863ac5', 'cyber_ammo_pistol_large', 0),
	(214, 'steam:11000010c863ac5', 'cupcake', 0),
	(215, 'steam:11000010c863ac5', 'coke', 0),
	(216, 'steam:11000010c863ac5', 'cokebrick', 0),
	(217, 'steam:11000010c863ac5', 'turtlebait', 0),
	(218, 'steam:11000010c863ac5', 'binoculars', 0),
	(219, 'steam:11000010c863ac5', 'lockpick', 0),
	(220, 'steam:11000010c863ac5', 'plastic', 0),
	(221, 'steam:11000010c863ac5', 'glass', 0),
	(222, 'steam:11000010c863ac5', 'aluminum', 0),
	(223, 'steam:11000010c863ac5', 'steel', 0),
	(224, 'steam:11000010c863ac5', 'armor', 0),
	(225, 'steam:11000010c863ac5', 'metalscrap', 0),
	(226, 'steam:11000010c863ac5', 'diamond', 0),
	(227, 'steam:11000010c863ac5', 'drill', 0),
	(228, 'steam:11000010c863ac5', 'goldwatch', 0),
	(229, 'steam:11000010c863ac5', 'copper', 0),
	(230, 'steam:11000010c863ac5', 'trimmedweed', 0),
	(231, 'steam:11000010c863ac5', 'rolex', 0),
	(232, 'steam:11000010c863ac5', 'redgull', 0),
	(233, 'steam:11000010c863ac5', 'bloodsample', 0),
	(234, 'steam:11000010c863ac5', 'lowgradefert', 0),
	(235, 'steam:11000010c863ac5', 'highgrademaleseed', 0),
	(236, 'steam:11000010c863ac5', 'pisswasser', 0),
	(237, 'steam:11000010c863ac5', 'accesscard', 0),
	(238, 'steam:11000010c863ac5', 'umbrella', 0),
	(239, 'steam:11000010c863ac5', 'oxygenmask', 0),
	(240, 'steam:11000010c863ac5', 'hackerDevice', 0),
	(241, 'steam:11000010c863ac5', 'dopebag', 0),
	(242, 'steam:11000010c863ac5', 'drugscales', 0),
	(243, 'steam:11000010c863ac5', 'scrap_metal', 0),
	(244, 'steam:11000010c863ac5', 'tacos', 0),
	(245, 'steam:11000010c863ac5', 'oxycutter', 0),
	(246, 'steam:11000010c863ac5', 'ammoanalyzer', 0),
	(247, 'steam:11000010c863ac5', 'lowgrademaleseed', 0),
	(248, 'steam:11000010c863ac5', 'purifiedwater', 0),
	(249, 'steam:11000010c863ac5', 'donut', 0),
	(250, 'steam:11000010c863ac5', 'sandwich', 0),
	(251, 'steam:11000010c863ac5', 'goldnecklace', 0),
	(252, 'steam:11000010c863ac5', 'bankidcard', 0),
	(253, 'steam:11000010c863ac5', 'highgradefert', 0),
	(254, 'steam:11000010c863ac5', 'aluminium', 0),
	(255, 'steam:11000010c863ac5', 'bagofdope', 0),
	(256, 'steam:11000010c863ac5', 'highgradefemaleseed', 0),
	(257, 'steam:11000010c863ac5', 'hammerwirecutter', 0),
	(258, 'steam:11000010c863ac5', 'bulletsample', 0),
	(259, 'steam:11000010c863ac5', 'wateringcan', 0),
	(260, 'steam:11000010c863ac5', 'rubber', 0),
	(261, 'steam:11000010c863ac5', 'electric_scrap', 0),
	(262, 'steam:11000010c863ac5', 'goldbar', 0),
	(263, 'steam:11000010c863ac5', 'plantpot', 0),
	(264, 'steam:11000010c863ac5', 'dnaanalyzer', 0),
	(265, 'steam:11000010c863ac5', 'lowgradefemaleseed', 0),
	(266, 'steam:11000010c863ac5', 'neckbrace', 0),
	(267, 'steam:11000010c863ac5', 'bodybandage', 0),
	(268, 'steam:11000010c863ac5', 'legbrace', 0),
	(269, 'steam:11000010c863ac5', 'joint', 0),
	(270, 'steam:11000010c863ac5', 'soda', 0),
	(271, 'steam:11000010c863ac5', 'armbrace', 0),
	(272, 'steam:11000010c863ac5', 'cigarette', 0);
/*!40000 ALTER TABLE `user_inventory` ENABLE KEYS */;

-- Dumping structure for table junsdb.user_lastcharacter
CREATE TABLE IF NOT EXISTS `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.user_lastcharacter: ~1 rows (approximately)
/*!40000 ALTER TABLE `user_lastcharacter` DISABLE KEYS */;
INSERT INTO `user_lastcharacter` (`steamid`, `charid`) VALUES
	('steam:11000010c863ac5', 2);
/*!40000 ALTER TABLE `user_lastcharacter` ENABLE KEYS */;

-- Dumping structure for table junsdb.user_licenses
CREATE TABLE IF NOT EXISTS `user_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(60) NOT NULL,
  `owner` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.user_licenses: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_licenses` ENABLE KEYS */;

-- Dumping structure for table junsdb.user_parkings
CREATE TABLE IF NOT EXISTS `user_parkings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) DEFAULT NULL,
  `garage` varchar(60) DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.user_parkings: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_parkings` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_parkings` ENABLE KEYS */;

-- Dumping structure for table junsdb.vehicles
CREATE TABLE IF NOT EXISTS `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  `stock` int(11) NOT NULL DEFAULT 10,
  PRIMARY KEY (`model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.vehicles: ~241 rows (approximately)
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` (`name`, `model`, `price`, `category`, `stock`) VALUES
	('Adder', 'adder', 900000, 'super', 10),
	('Akuma', 'AKUMA', 7500, 'motorcycles', 10),
	('Alpha', 'alpha', 60000, 'sports', 10),
	('Ardent', 'ardent', 1150000, 'sportsclassics', 10),
	('Asea', 'asea', 5500, 'sedans', 10),
	('Autarch', 'autarch', 1955000, 'super', 10),
	('Avarus', 'avarus', 18000, 'motorcycles', 10),
	('Bagger', 'bagger', 13500, 'motorcycles', 10),
	('Baller', 'baller2', 40000, 'suvs', 10),
	('Baller Sport', 'baller3', 60000, 'suvs', 10),
	('Banshee', 'banshee', 70000, 'sports', 10),
	('Banshee 900R', 'banshee2', 255000, 'super', 10),
	('Bati 801', 'bati', 12000, 'motorcycles', 10),
	('Bati 801RR', 'bati2', 19000, 'motorcycles', 10),
	('Bestia GTS', 'bestiagts', 55000, 'sports', 10),
	('BF400', 'bf400', 6500, 'motorcycles', 10),
	('Bf Injection', 'bfinjection', 16000, 'offroad', 10),
	('Bifta', 'bifta', 12000, 'offroad', 10),
	('Bison', 'bison', 45000, 'vans', 10),
	('Blade', 'blade', 15000, 'muscle', 10),
	('Blazer', 'blazer', 6500, 'offroad', 10),
	('Blazer Sport', 'blazer4', 8500, 'offroad', 10),
	('blazer5', 'blazer5', 1755600, 'offroad', 10),
	('Blista', 'blista', 8000, 'compacts', 10),
	('BMX (velo)', 'bmx', 160, 'bicycles', 10),
	('Bobcat XL', 'bobcatxl', 32000, 'vans', 10),
	('Brawler', 'brawler', 45000, 'offroad', 10),
	('Brioso R/A', 'brioso', 18000, 'compacts', 10),
	('Btype', 'btype', 62000, 'sportsclassics', 10),
	('Btype Hotroad', 'btype2', 155000, 'sportsclassics', 10),
	('Btype Luxe', 'btype3', 85000, 'sportsclassics', 10),
	('Buccaneer', 'buccaneer', 18000, 'muscle', 10),
	('Buccaneer Rider', 'buccaneer2', 24000, 'muscle', 10),
	('Buffalo', 'buffalo', 12000, 'sports', 10),
	('Buffalo S', 'buffalo2', 20000, 'sports', 10),
	('Bullet', 'bullet', 90000, 'super', 10),
	('Burrito', 'burrito3', 19000, 'vans', 10),
	('Camper', 'camper', 42000, 'vans', 10),
	('Carbonizzare', 'carbonizzare', 75000, 'sports', 10),
	('Carbon RS', 'carbonrs', 18000, 'motorcycles', 10),
	('Casco', 'casco', 30000, 'sportsclassics', 10),
	('Cavalcade', 'cavalcade2', 55000, 'suvs', 10),
	('Cheetah', 'cheetah', 375000, 'super', 10),
	('Chimera', 'chimera', 38000, 'motorcycles', 10),
	('Chino', 'chino', 15000, 'muscle', 10),
	('Chino Luxe', 'chino2', 19000, 'muscle', 10),
	('Cliffhanger', 'cliffhanger', 9500, 'motorcycles', 10),
	('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes', 10),
	('Cognoscenti', 'cognoscenti', 55000, 'sedans', 10),
	('Comet', 'comet2', 65000, 'sports', 10),
	('Comet 5', 'comet5', 1145000, 'sports', 10),
	('Contender', 'contender', 70000, 'suvs', 10),
	('Coquette', 'coquette', 65000, 'sports', 10),
	('Coquette Classic', 'coquette2', 40000, 'sportsclassics', 10),
	('Coquette BlackFin', 'coquette3', 55000, 'muscle', 10),
	('Cruiser (velo)', 'cruiser', 510, 'bicycles', 10),
	('Cyclone', 'cyclone', 1890000, 'super', 10),
	('Daemon', 'daemon', 11500, 'motorcycles', 10),
	('Daemon High', 'daemon2', 13500, 'motorcycles', 10),
	('Defiler', 'defiler', 9800, 'motorcycles', 10),
	('Deluxo', 'deluxo', 4721500, 'sportsclassics', 10),
	('Dominator', 'dominator', 35000, 'muscle', 10),
	('Double T', 'double', 28000, 'motorcycles', 10),
	('Dubsta', 'dubsta', 45000, 'suvs', 10),
	('Dubsta Luxuary', 'dubsta2', 60000, 'suvs', 10),
	('Bubsta 6x6', 'dubsta3', 120000, 'offroad', 10),
	('Dukes', 'dukes', 28000, 'muscle', 10),
	('Dune Buggy', 'dune', 8000, 'offroad', 10),
	('Elegy', 'elegy2', 38500, 'sports', 10),
	('Emperor', 'emperor', 8500, 'sedans', 10),
	('Enduro', 'enduro', 5500, 'motorcycles', 10),
	('Entity XF', 'entityxf', 425000, 'super', 10),
	('Esskey', 'esskey', 4200, 'motorcycles', 10),
	('Exemplar', 'exemplar', 32000, 'coupes', 10),
	('F620', 'f620', 40000, 'coupes', 10),
	('Faction', 'faction', 20000, 'muscle', 10),
	('Faction Rider', 'faction2', 30000, 'muscle', 10),
	('Faction XL', 'faction3', 40000, 'muscle', 10),
	('Faggio', 'faggio', 1900, 'motorcycles', 10),
	('Vespa', 'faggio2', 2800, 'motorcycles', 10),
	('Felon', 'felon', 42000, 'coupes', 10),
	('Felon GT', 'felon2', 55000, 'coupes', 10),
	('Feltzer', 'feltzer2', 55000, 'sports', 10),
	('Stirling GT', 'feltzer3', 65000, 'sportsclassics', 10),
	('Fixter (velo)', 'fixter', 225, 'bicycles', 10),
	('Tow Truck', 'flatbed', 625000, 'utility', 10),
	('FMJ', 'fmj', 185000, 'super', 10),
	('Fhantom', 'fq2', 17000, 'suvs', 10),
	('Fugitive', 'fugitive', 12000, 'sedans', 10),
	('Furore GT', 'furoregt', 45000, 'sports', 10),
	('Fusilade', 'fusilade', 40000, 'sports', 10),
	('Gargoyle', 'gargoyle', 16500, 'motorcycles', 10),
	('Gauntlet', 'gauntlet', 30000, 'muscle', 10),
	('Gang Burrito', 'gburrito', 45000, 'vans', 10),
	('Burrito', 'gburrito2', 29000, 'vans', 10),
	('Glendale', 'glendale', 6500, 'sedans', 10),
	('Grabger', 'granger', 50000, 'suvs', 10),
	('Gresley', 'gresley', 47500, 'suvs', 10),
	('GT 500', 'gt500', 785000, 'sportsclassics', 10),
	('Guardian', 'guardian', 45000, 'offroad', 10),
	('Hakuchou', 'hakuchou', 31000, 'motorcycles', 10),
	('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles', 10),
	('Hermes', 'hermes', 535000, 'muscle', 10),
	('Hexer', 'hexer', 12000, 'motorcycles', 10),
	('Hotknife', 'hotknife', 125000, 'muscle', 10),
	('Huntley S', 'huntley', 40000, 'suvs', 10),
	('Hustler', 'hustler', 625000, 'muscle', 10),
	('Infernus', 'infernus', 180000, 'super', 10),
	('Innovation', 'innovation', 23500, 'motorcycles', 10),
	('Intruder', 'intruder', 7500, 'sedans', 10),
	('Issi', 'issi2', 10000, 'compacts', 10),
	('Jackal', 'jackal', 38000, 'coupes', 10),
	('Jester', 'jester', 65000, 'sports', 10),
	('Jester(Racecar)', 'jester2', 135000, 'sports', 10),
	('Journey', 'journey', 6500, 'vans', 10),
	('Kamacho', 'kamacho', 345000, 'offroad', 10),
	('Khamelion', 'khamelion', 38000, 'sports', 10),
	('Kuruma', 'kuruma', 30000, 'sports', 10),
	('Landstalker', 'landstalker', 35000, 'suvs', 10),
	('RE-7B', 'le7b', 325000, 'super', 10),
	('Lynx', 'lynx', 40000, 'sports', 10),
	('Mamba', 'mamba', 70000, 'sports', 10),
	('Manana', 'manana', 12800, 'sportsclassics', 10),
	('Manchez', 'manchez', 5300, 'motorcycles', 10),
	('Massacro', 'massacro', 65000, 'sports', 10),
	('Massacro(Racecar)', 'massacro2', 130000, 'sports', 10),
	('Mesa', 'mesa', 16000, 'suvs', 10),
	('Mesa Trail', 'mesa3', 40000, 'suvs', 10),
	('Minivan', 'minivan', 13000, 'vans', 10),
	('Monroe', 'monroe', 55000, 'sportsclassics', 10),
	('The Liberator', 'monster', 210000, 'offroad', 10),
	('Moonbeam', 'moonbeam', 18000, 'vans', 10),
	('Moonbeam Rider', 'moonbeam2', 35000, 'vans', 10),
	('Nemesis', 'nemesis', 5800, 'motorcycles', 10),
	('Neon', 'neon', 1500000, 'sports', 10),
	('Nightblade', 'nightblade', 35000, 'motorcycles', 10),
	('Nightshade', 'nightshade', 65000, 'muscle', 10),
	('9F', 'ninef', 65000, 'sports', 10),
	('9F Cabrio', 'ninef2', 80000, 'sports', 10),
	('Omnis', 'omnis', 35000, 'sports', 10),
	('Oppressor', 'oppressor', 3524500, 'super', 10),
	('Oracle XS', 'oracle2', 35000, 'coupes', 10),
	('Osiris', 'osiris', 160000, 'super', 10),
	('Panto', 'panto', 10000, 'compacts', 10),
	('Paradise', 'paradise', 19000, 'vans', 10),
	('Pariah', 'pariah', 1420000, 'sports', 10),
	('Patriot', 'patriot', 55000, 'suvs', 10),
	('PCJ-600', 'pcj', 6200, 'motorcycles', 10),
	('Penumbra', 'penumbra', 28000, 'sports', 10),
	('Pfister', 'pfister811', 85000, 'super', 10),
	('Phoenix', 'phoenix', 12500, 'muscle', 10),
	('Picador', 'picador', 18000, 'muscle', 10),
	('Pigalle', 'pigalle', 20000, 'sportsclassics', 10),
	('Prairie', 'prairie', 12000, 'compacts', 10),
	('Premier', 'premier', 8000, 'sedans', 10),
	('Primo Custom', 'primo2', 14000, 'sedans', 10),
	('X80 Proto', 'prototipo', 2500000, 'super', 10),
	('Radius', 'radi', 29000, 'suvs', 10),
	('raiden', 'raiden', 1375000, 'sports', 10),
	('Rapid GT', 'rapidgt', 35000, 'sports', 10),
	('Rapid GT Convertible', 'rapidgt2', 45000, 'sports', 10),
	('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics', 10),
	('Reaper', 'reaper', 150000, 'super', 10),
	('Rebel', 'rebel2', 35000, 'offroad', 10),
	('Regina', 'regina', 5000, 'sedans', 10),
	('Retinue', 'retinue', 615000, 'sportsclassics', 10),
	('Revolter', 'revolter', 1610000, 'sports', 10),
	('riata', 'riata', 380000, 'offroad', 10),
	('Rocoto', 'rocoto', 45000, 'suvs', 10),
	('Ruffian', 'ruffian', 6800, 'motorcycles', 10),
	('Ruiner 2', 'ruiner2', 5745600, 'muscle', 10),
	('Rumpo', 'rumpo', 15000, 'vans', 10),
	('Rumpo Trail', 'rumpo3', 19500, 'vans', 10),
	('Sabre Turbo', 'sabregt', 20000, 'muscle', 10),
	('Sabre GT', 'sabregt2', 25000, 'muscle', 10),
	('Sanchez', 'sanchez', 5300, 'motorcycles', 10),
	('Sanchez Sport', 'sanchez2', 5300, 'motorcycles', 10),
	('Sanctus', 'sanctus', 25000, 'motorcycles', 10),
	('Sandking', 'sandking', 55000, 'offroad', 10),
	('Savestra', 'savestra', 990000, 'sportsclassics', 10),
	('SC 1', 'sc1', 1603000, 'super', 10),
	('Schafter', 'schafter2', 25000, 'sedans', 10),
	('Schafter V12', 'schafter3', 50000, 'sports', 10),
	('Scorcher (velo)', 'scorcher', 280, 'bicycles', 10),
	('Seminole', 'seminole', 25000, 'suvs', 10),
	('Sentinel', 'sentinel', 32000, 'coupes', 10),
	('Sentinel XS', 'sentinel2', 40000, 'coupes', 10),
	('Sentinel3', 'sentinel3', 650000, 'sports', 10),
	('Seven 70', 'seven70', 39500, 'sports', 10),
	('ETR1', 'sheava', 220000, 'super', 10),
	('Shotaro Concept', 'shotaro', 320000, 'motorcycles', 10),
	('Slam Van', 'slamvan3', 11500, 'muscle', 10),
	('Sovereign', 'sovereign', 22000, 'motorcycles', 10),
	('Stinger', 'stinger', 80000, 'sportsclassics', 10),
	('Stinger GT', 'stingergt', 75000, 'sportsclassics', 10),
	('Streiter', 'streiter', 500000, 'sports', 10),
	('Stretch', 'stretch', 90000, 'sedans', 10),
	('Stromberg', 'stromberg', 3185350, 'sports', 10),
	('Sultan', 'sultan', 15000, 'sports', 10),
	('Sultan RS', 'sultanrs', 65000, 'super', 10),
	('Super Diamond', 'superd', 130000, 'sedans', 10),
	('Surano', 'surano', 50000, 'sports', 10),
	('Surfer', 'surfer', 12000, 'vans', 10),
	('T20', 't20', 300000, 'super', 10),
	('Tailgater', 'tailgater', 30000, 'sedans', 10),
	('Tampa', 'tampa', 16000, 'muscle', 10),
	('Drift Tampa', 'tampa2', 80000, 'sports', 10),
	('Thrust', 'thrust', 24000, 'motorcycles', 10),
	('Tri bike (velo)', 'tribike3', 520, 'bicycles', 10),
	('Trophy Truck', 'trophytruck', 60000, 'offroad', 10),
	('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad', 10),
	('Tropos', 'tropos', 40000, 'sports', 10),
	('Turismo R', 'turismor', 350000, 'super', 10),
	('Tyrus', 'tyrus', 600000, 'super', 10),
	('Vacca', 'vacca', 120000, 'super', 10),
	('Vader', 'vader', 7200, 'motorcycles', 10),
	('Verlierer', 'verlierer2', 70000, 'sports', 10),
	('Vigero', 'vigero', 12500, 'muscle', 10),
	('Virgo', 'virgo', 14000, 'muscle', 10),
	('Viseris', 'viseris', 875000, 'sportsclassics', 10),
	('Visione', 'visione', 2250000, 'super', 10),
	('Voltic', 'voltic', 90000, 'super', 10),
	('Voltic 2', 'voltic2', 3830400, 'super', 9),
	('Voodoo', 'voodoo', 7200, 'muscle', 10),
	('Vortex', 'vortex', 9800, 'motorcycles', 10),
	('Warrener', 'warrener', 4000, 'sedans', 10),
	('Washington', 'washington', 9000, 'sedans', 10),
	('Windsor', 'windsor', 95000, 'coupes', 10),
	('Windsor Drop', 'windsor2', 125000, 'coupes', 10),
	('Woflsbane', 'wolfsbane', 9000, 'motorcycles', 10),
	('XLS', 'xls', 32000, 'suvs', 10),
	('Yosemite', 'yosemite', 485000, 'muscle', 10),
	('Youga', 'youga', 10800, 'vans', 10),
	('Youga Luxuary', 'youga2', 14500, 'vans', 10),
	('Z190', 'z190', 900000, 'sportsclassics', 10),
	('Zentorno', 'zentorno', 1500000, 'super', 9),
	('Zion', 'zion', 36000, 'coupes', 10),
	('Zion Cabrio', 'zion2', 45000, 'coupes', 10),
	('Zombie', 'zombiea', 9500, 'motorcycles', 10),
	('Zombie Luxuary', 'zombieb', 12000, 'motorcycles', 10),
	('Z-Type', 'ztype', 220000, 'sportsclassics', 10);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;

-- Dumping structure for table junsdb.vehicle_categories
CREATE TABLE IF NOT EXISTS `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.vehicle_categories: ~13 rows (approximately)
/*!40000 ALTER TABLE `vehicle_categories` DISABLE KEYS */;
INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
	('bicycles', 'Bikes'),
	('compacts', 'Compacts'),
	('coupes', 'Coupes'),
	('motorcycles', 'Motorcycles'),
	('muscle', 'Muscle'),
	('offroad', 'Off Road'),
	('sedans', 'Sedans'),
	('sports', 'Sports'),
	('sportsclassics', 'Sports Classics'),
	('super', 'Super'),
	('suvs', 'SUVs'),
	('utility', 'Utility'),
	('vans', 'Vans');
/*!40000 ALTER TABLE `vehicle_categories` ENABLE KEYS */;

-- Dumping structure for table junsdb.vehicle_display
CREATE TABLE IF NOT EXISTS `vehicle_display` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(60) NOT NULL,
  `name` varchar(60) NOT NULL,
  `commission` int(11) NOT NULL DEFAULT 10,
  `downpayment` int(11) NOT NULL DEFAULT 25,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.vehicle_display: ~9 rows (approximately)
/*!40000 ALTER TABLE `vehicle_display` DISABLE KEYS */;
INSERT INTO `vehicle_display` (`id`, `model`, `name`, `commission`, `downpayment`) VALUES
	(1, 'panto', 'Panto', 10, 25),
	(2, 'carbonrs', 'Carbon RS', 10, 25),
	(3, 'ardent', 'Ardent', 10, 25),
	(4, 'voltic2', 'Voltic 2', 10, 25),
	(5, 'exemplar', 'Exemplar', 10, 25),
	(6, 'emperor', 'Emperor', 10, 25),
	(7, 'baller2', 'Baller', 10, 25),
	(8, 'fixter', 'Fixter (velo)', 10, 25),
	(9, 'trophytruck', 'Trophy Truck', 10, 25);
/*!40000 ALTER TABLE `vehicle_display` ENABLE KEYS */;

-- Dumping structure for table junsdb.vehicle_sold
CREATE TABLE IF NOT EXISTS `vehicle_sold` (
  `client` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `plate` varchar(50) NOT NULL,
  `soldby` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  PRIMARY KEY (`plate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table junsdb.vehicle_sold: ~0 rows (approximately)
/*!40000 ALTER TABLE `vehicle_sold` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_sold` ENABLE KEYS */;

-- Dumping structure for table junsdb.weashops
CREATE TABLE IF NOT EXISTS `weashops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- Dumping data for table junsdb.weashops: ~40 rows (approximately)
/*!40000 ALTER TABLE `weashops` DISABLE KEYS */;
INSERT INTO `weashops` (`id`, `zone`, `item`, `price`) VALUES
	(1, 'GunShop', 'WEAPON_PISTOL', 300),
	(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
	(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
	(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
	(5, 'GunShop', 'WEAPON_MACHETE', 90),
	(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
	(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
	(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
	(9, 'GunShop', 'WEAPON_BAT', 100),
	(10, 'BlackWeashop', 'WEAPON_BAT', 100),
	(11, 'GunShop', 'WEAPON_STUNGUN', 50),
	(12, 'BlackWeashop', 'WEAPON_STUNGUN', 50),
	(13, 'GunShop', 'WEAPON_MICROSMG', 1400),
	(14, 'BlackWeashop', 'WEAPON_MICROSMG', 1700),
	(15, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3400),
	(16, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500),
	(17, 'GunShop', 'WEAPON_ASSAULTRIFLE', 10000),
	(18, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000),
	(19, 'GunShop', 'WEAPON_SPECIALCARBINE', 15000),
	(20, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500),
	(21, 'GunShop', 'WEAPON_SNIPERRIFLE', 22000),
	(22, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000),
	(23, 'GunShop', 'WEAPON_FIREWORK', 18000),
	(24, 'BlackWeashop', 'WEAPON_FIREWORK', 20000),
	(25, 'GunShop', 'WEAPON_GRENADE', 500),
	(26, 'BlackWeashop', 'WEAPON_GRENADE', 650),
	(27, 'GunShop', 'WEAPON_BZGAS', 200),
	(28, 'BlackWeashop', 'WEAPON_BZGAS', 350),
	(29, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
	(30, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
	(31, 'GunShop', 'WEAPON_BALL', 50),
	(32, 'BlackWeashop', 'WEAPON_BALL', 50),
	(33, 'GunShop', 'WEAPON_SMOKEGRENADE', 100),
	(34, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
	(35, 'BlackWeashop', 'WEAPON_APPISTOL', 1100),
	(36, 'BlackWeashop', 'WEAPON_CARBINERIFLE', 12000),
	(37, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000),
	(38, 'BlackWeashop', 'WEAPON_MINIGUN', 45000),
	(39, 'BlackWeashop', 'WEAPON_RAILGUN', 50000),
	(40, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500);
/*!40000 ALTER TABLE `weashops` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
